"""
Author: Agnes Valenti
Date: 01.03.2021
"""

from __future__ import division
from __future__ import absolute_import
from __future__ import print_function

import sys
sys.path.append('../../network/')


import matplotlib
matplotlib.use('Agg')

import warnings
warnings.filterwarnings('ignore',category=FutureWarning)


import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np
import network as net






saver=tf.train.Saver()
sess = tf.Session()
sess.run(tf.global_variables_initializer())

##########################################################################################################


if True:
    #load density correlator expectation values
    pdata=np.loadtxt('../data/data_expvalues.txt')
    pdata=np.reshape(pdata,(1,171))
    
    #load correct value U_8
    plabels=np.loadtxt('../data/uvalues_correct.txt')[7]
    
    #load pre-trained network
    tf.reset_default_graph()
    saver.restore(sess, '../../network/checkpoints_2500shots/pretrained_u8/-2099900')

    #run the network estimation
    pbetas = sess.run([net.beta_output], feed_dict={net.tf_x: pdata})
    pbetas=np.reshape(pbetas,(np.shape(plabels)))
    
    print("\n")
    print("\n")
    print("Correct value U_8=",plabels)
    print("Estimated value U_8=",pbetas)
    print("U_8 estimation error=",np.abs(plabels-pbetas))
    print("\n")
    print("\n")

    #save [correct value, estimated value, estimation error] in the file 'params_u8.txt'
    np.savetxt('../params_u8.txt',np.array([plabels,pbetas,np.abs(plabels-pbetas)]))

