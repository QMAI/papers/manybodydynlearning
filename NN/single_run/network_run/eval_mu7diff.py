"""
Author: Agnes Valenti
Date: 01.03.2021
"""

from __future__ import division
from __future__ import absolute_import
from __future__ import print_function

import sys
sys.path.append('../../network/')


import matplotlib
matplotlib.use('Agg')

import warnings
warnings.filterwarnings('ignore',category=FutureWarning)


import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np
import network as net






saver=tf.train.Saver()
sess = tf.Session()
sess.run(tf.global_variables_initializer())

##########################################################################################################


if True:
    #load density correlator expectation values
    pdata=np.loadtxt('../data/data_expvalues.txt')
    pdata=np.reshape(pdata,(1,171))
    
    #load correct value mu_7-mu_1
    plabels=np.loadtxt('../data/muvalues_correct.txt')[6]-np.loadtxt('../data/muvalues_correct.txt')[0]
    
    #load pre-trained network
    tf.reset_default_graph()
    saver.restore(sess, '../../network/checkpoints_2500shots/pretrained_mu7diff/-2099900')

    #run the network estimation
    pbetas = sess.run([net.beta_output], feed_dict={net.tf_x: pdata})
    pbetas=np.reshape(pbetas,(np.shape(plabels)))
    
    print("\n")
    print("\n")
    print("Correct value mu_7-mu_1=",plabels)
    print("Estimated value mu_7-mu_1=",pbetas)
    print("mu_7-mu_1 estimation error=",np.abs(plabels-pbetas))
    print("\n")
    print("\n")

    #save [correct value, estimated value, estimation error] in the file 'params_mu7diff.txt'
    np.savetxt('../params_mu7diff.txt',np.array([plabels,pbetas,np.abs(plabels-pbetas)]))

