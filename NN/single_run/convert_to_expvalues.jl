"""
Author: Agnes Valenti, Guliuxin Jin
Date: 01.03.2021

This code generates converts 'mt' snapshots from the file 'snapshots.txt' into 171 density correlation expectation values.
"""

using LinearAlgebra, Random, DelimitedFiles


#  N: number of atoms, M: number of sites
hbar = 1 ;  N = 4 ;   M = 8 ; 

#  1 set of measurements, each set contains 2500 snapshots
measure_set= 1; mt = 2500; 


print("Convert measurement snapshots to density correlation expectation values...","\n","\n")

function basis(N,M)  # collection of all Fock basis for N particles in M lattices.
    """Define the fock basis collection, collection of all Fock basis states for N particles on M lattice sites """

    #how many different possible configurations for N particles, M lattice sites
    hbt=binomial(M+N-1,M-1)  
# --------------------     function basis(s,hbt,bss)   ------------------					--    
    bss_outcome = []
    a = zeros(Int64,M)
    for i in 1:(N+1)^M
        if sum(a)==N
            b=copy(a)
            push!(bss_outcome,b)
        end        
        a[M]+=1
        for j in 0:M-2
            if a[M-j]==N+1
                a[M-j]=0
                a[M-j-1] +=1
            end 
        end
    end
    bss_outcome
# --------------------     function MB_state(s,hbt,bss)   --------------------
    bsslenth = length(bss_outcome) 
    fullmb = []
    for s in 1:bsslenth
        mbstate = zeros(Int64, hbt)
        mbstate[s]=1
        push!(fullmb,mbstate)
    end
    return bss_outcome
end

#save Fock state configurations into an array bss, where bss[n,:] corresponds to the atom configuration of the n'th Fock state
bss = basis(N,M); 


function n_(i,S)
  """returns occupation number n_i on the i'th site of the Fock state S"""
  return bss[S][i]
end

function nsquared_(i,S)
  """returns the product of occupation numbers n_i(n_i-1)"""
  return bss[S][i]*(bss[S][i]-1)
end

function two_dens_corr_(i,j,S)
  """returns the density correlation n_i*n_j"""
  return bss[S][i]*bss[S][j]
end

function three_dens_corr_(i,j,k,S)
  """returns the density correlation n_i*n_j*n_k"""
  return bss[S][i]*bss[S][j]*bss[S][k]
end

function four_dens_corr_(i,j,k,l,S)
  """returns the density correlation n_i*n_j*n_k*n_l"""
  return bss[S][i]*bss[S][j]*bss[S][k]*bss[S][l]
end

function two_nsquared_(i,j,S)
  """returns the density correlation n_i(n_i-1)*n_j*(n_j-1)"""
  return bss[S][i]*(bss[S][i]-1)*bss[S][j]*(bss[S][j]-1)
end


function exp_density(i,measurements)
  """returns the expectation value <n_i>"""
  dens=sum(n_(i,measurements[m]) for m in 1:length(measurements))
  dens=dens./length(measurements)
  return dens
end

function exp_denssquared(i,measurements)
  """returns the expectation value <n_i(n_i-1)>"""
  dens=sum(nsquared_(i,measurements[m]) for m in 1:length(measurements))
  dens=dens./length(measurements)
  return dens
end

function exp_two_dens_corr(i,j,measurements)
  """returns the expectation value <n_i*n_j>"""
  dens=sum(two_dens_corr_(i,j,measurements[m]) for m in 1:length(measurements))
  dens=dens./length(measurements)
  return dens
end

function exp_three_dens_corr(i,j,k,measurements)
  """returns the expectation value <n_i*n_j*n_k>"""
  dens=sum(three_dens_corr_(i,j,k,measurements[m]) for m in 1:length(measurements))
  dens=dens./length(measurements)
  return dens
end

function exp_two_nsquared(i,j,measurements)
  """returns the expectation value <n_i(n_i-1)*n_j*(n_j-1)>"""
  dens=sum(two_nsquared_(i,j,measurements[m]) for m in 1:length(measurements))
  dens=dens./length(measurements)
  return dens
end

function exp_four_dens_corr(i,j,k,l,measurements)
  """returns the expectation value <n_i*n_j*n_k*n_l>"""
  dens=sum(four_dens_corr_(i,j,k,l,measurements[m]) for m in 1:length(measurements))
  dens=dens./length(measurements)
  return dens
end




num_measurements=1
num_exp=171
expvalues=zeros(Float64,(num_measurements,num_exp))
jvalues_ges=zeros(Float64,(num_measurements,8*8))
uvalues_ges=zeros(Float64,(num_measurements,8))
muvalues_ges=zeros(Float64,(num_measurements,8))
labels=zeros(Float64,num_measurements)

#calculate 199 expectation values from the generated snapshots
for meas_i in 1:num_measurements
  measurements = readdlm("data/snapshots.txt", Int64)
  index=1
  for i in 1:8
    expvalues[meas_i,index]=exp_density(i,measurements)
    index+=1
  end
 
  for i in 1:8
    expvalues[meas_i,index]=exp_denssquared(i,measurements)
    index+=1
  end
  
  for i in 1:8
    for j in (i+1):8
      expvalues[meas_i,index]=exp_two_dens_corr(i,j,measurements)
      index+=1
    end
  end

  for i in 1:8
    for j in (i+1):8
      for k in (j+1):8
        if 1==1
          expvalues[meas_i,index]=exp_three_dens_corr(i,j,k,measurements)
          index+=1
        end
      end
    end
  end

  for i in 1:8
    for j in (i+1):8
      for k in (j+1):8
        for l in (k+1):8
          expvalues[meas_i,index]=exp_four_dens_corr(i,j,k,l,measurements)
          index+=1
        end
      end
    end
  end
end

#save expectation values
print("\n","Data converted.","\n","\n")
writedlm("data/data_expvalues.txt", expvalues)


