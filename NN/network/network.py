"""
Author: Agnes Valenti
Date: 01.03.2021
"""

import tensorflow as tf

from tensorflow.python.util import deprecation
deprecation._PRINT_DEPRECATION_WARNINGS = False

#learning rate used for training: here not important
LR=0.00001

#----------------------------------------------------------------------------------------------
#network

#density correlator expectation value input
tf_x = tf.placeholder(tf.float32, [None, 171])    # value in the range of (0, 1)

#label: parameter strength (can be used for hopping strengths, on-site repulsions and chemical potential differences
tf_y_beta=tf.placeholder(tf.float32, [None,1])

#hidden layers
en0 = tf.layers.dense(tf_x, 230, tf.nn.relu,name='en0_')  
en10 = tf.layers.dense(en0, 300, tf.nn.relu)
en1 = tf.layers.dense(en10, 400, tf.nn.relu)     
en2 = tf.layers.dense(en1, 300, tf.nn.relu)     
en3 = tf.layers.dense(en2, 150, tf.nn.relu)     
en4 = tf.layers.dense(en3, 100, tf.nn.relu)     

#output neuron
beta_output=tf.layers.dense(en4, 1)

tf_y_beta2=tf.cast(tf_y_beta,tf.float64)

#calculate loss of continuous parameter as mean squared error
loss=tf.losses.mean_squared_error(tf_y_beta2,beta_output)

predictions=beta_output

train = tf.train.AdamOptimizer(LR).minimize(loss)


