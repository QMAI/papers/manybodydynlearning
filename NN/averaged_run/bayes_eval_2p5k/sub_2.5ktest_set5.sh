#!/bin/bash -l
#SBATCH --job-name="2.5ktest_set5"
#SBATCH --mail-type=ALL
#SBATCH --mail-user=gjin@student.ethz.ch
#SBATCH --time=18:59:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-core=1
#SBATCH --ntasks-per-node=9
#SBATCH --cpus-per-task=4
#SBATCH --partition=normal
#SBATCH --constraint=mc
#SBATCH --hint=nomultithread
#SBATCH --output=2.5ktest_set5_%j.out     # Standard output and error log

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export JULIA_NUM_THREADS=$SLURM_CPUS_PER_TASK


#-----------------------
# script
#-----------------------


echo "Starting job!!! ${SLURM_JOB_ID}"

# print out environment variables related to SLURM_NTASKS
# julia -e 'println("\n"); [println((k,ENV[k],)) for k in keys(ENV) if occursin("SLURM_NTASKS_PER_NODE",k)]; println("\n");'

# run the script
# julia --optimize=3 mc test-script.jl
julia --optimize=3  2.5ktest_set5.jl
