# detect if using SLURM
const IN_SLURM = "SLURM_JOBID" in keys(ENV)

# load packages
using Distributed
IN_SLURM && using ClusterManagers

# Here we create our parallel julia processes
if IN_SLURM
    pids = addprocs_slurm(parse(Int, ENV["SLURM_NTASKS"]))
    print("\n")
else
    pids = addprocs()
end
println("workers = ", workers())

@everywhere using LinearAlgebra, Random, DelimitedFiles

###################################################################
##################                             ####################
##################     Declare  parameters     ####################
##################                             ####################
###################################################################

set_num = 5  # which set it is.
tot_run_num = 2 # how many iterations of J-U-J-U.. here is 2.

hbar = 1 ;  N = 4 ;   M = 8 ;  
t_evl = 200*hbar;  ini_st = 150;  # initial state = Fock state No.150
measure_set= 1; mt = 2500; # total num of measure  = measure_set*mt = 2.5k 

j_shares = 12; ### num of candidates: shares+1
u_shares = 20; ### how many shares do we have in the partition of candidate interval


iterations = 8;  # this is the iteration over all J or U groups. i.e. 8 iteration = run all j groups 8 times.
precision = [0.02 for i in 1:iterations] ####   precision of estimation
j_boxes = [[1,2],[1,5],[5,6],[6,2]], [[2,3],[3,7],[7,6],[6,2]], [[3,4],[4,8],[8,7],[7,3]];
u_groups = [ [1,5],[2,6],[3,7],[4,8],[1,2],[3,4],[5,6],[7,8] ];


###################################################################
##################                             ####################
##################       Declare functions     ####################
##################                             ####################
###################################################################

### Function 1: Define the fock basis collection
function basis(N,M)  # collection of all Fock basis for N particles in M lattices.
    hbt=binomial(M+N-1,M-1)
    bss_outcome = []
    a = zeros(Int64,M)
    for i in 1:(N+1)^M
        if sum(a)==N
            b=copy(a)
            push!(bss_outcome,b)
        end        
        a[M]+=1
        for j in 0:M-2
            if a[M-j]==N+1
                a[M-j]=0
                a[M-j-1] +=1
            end 
        end
    end
# --------------------     function MB_state(s,hbt,bss)   --------------------
    bsslenth = length(bss_outcome) 
    fullmb = []
    for s in 1:bsslenth
        mbstate = zeros(Int64, hbt)
        mbstate[s]=1
        push!(fullmb,mbstate)
    end
    return bss_outcome, fullmb
end
bss, MB_state = basis(N,M); ##### this is one potential global variables!

#################################################################
### Function 2: generate all the parameter configurations. 
### variables contain the 2% / 1.5% / 1% ...

function full_parm_j(;j_full, j_boxes, precise, gn, shares ) # specify a parameter group by a leading J_{ij}
    # gn:  group number: tells which group we are constrcuting all parms candidates.
    #  ------------   candidates generator  -----------
    partition = [i for i in 1-precise/2:precise/shares:1+precise/2]  ## devide the [0.995-1.005] into 9 shares.
    group = j_boxes[gn];
    cand = [];
    for i in group
        a,b = i
        candid_j = partition.*j_full[a,b]   #j_full[j,l] should equal to j_full[l,j]
        println("for j_full[$a,$b] = ",j_full[a,b], ". candidate for J_$a $b are ",candid_j)
        push!(cand,candid_j)
    end
    #  ------------   candidates generator end  ,  candidates combination start   -----------
    j_total = []; 
    p_n_each = length(cand[1]) # number of candidates for each J_ij
    
    
    for q in 1:p_n_each, p in 1:p_n_each, m in 1:p_n_each, n in 1:p_n_each 
        j_out = copy(j_full);
        j_out[ group[1][1], group[1][2]] = j_out[ group[1][2], group[1][1]] = cand[1][q]
        j_out[ group[2][1], group[2][2]] = j_out[ group[2][2], group[2][1]] = cand[2][p]
        j_out[ group[3][1], group[3][2]] = j_out[ group[3][2], group[3][1]] = cand[3][m]
        j_out[ group[4][1], group[4][2]] = j_out[ group[4][2], group[4][1]] = cand[4][n]
        push!(j_total,j_out);
    end
    return j_total
end


function full_parm_u(;u_full, u_groups, precise, gn, shares ) # specify a parameter group by a leading J_{ij}
    # gn:  group number: tells which group we are constrcuting all parms candidates.
    #  ------------   candidates generator  -----------
    partition = [i for i in 1-precise/2:precise/shares:1+precise/2]  ## devide the [0.995-1.005] into 11 shares.
    group = u_groups[gn];
    cand = [];
    for j in group  # group[1] is the position of U and mu
        candid_u = [];
        candid_u = partition.*u_full[j];
        push!(cand, candid_u);  # variable cand contains 4 sets of candidates for mu in a group. 
    end
    for ii in 1:length(group)
        print("\n the candidate of U[$(group[ii])] are: ", cand[ii])
    end
    #  ------------   candidates generator end  ,  candidates combination start   -----------
    u_total = []
    p_n_each = length(cand[1])                              # number of candidates for each  u_i
    #    --------------------------------------------------------------
    for n in 1:p_n_each, m in 1:p_n_each
        u_out = copy(u_full);                               # parms outside this group,use the value stored here        
        u_out[group[1]] = cand[1][n];
        u_out[group[2]] = cand[2][m];
#         u_out[group[3]] = cand[3][p];
#         u_out[group[4]] = cand[4][q];
        push!(u_total,u_out);
    end
    return u_total
end

# u_total contains (shares+1)^4  elements.
###################################################################################
### Function 3: probs of given parms[i]. only evaluate 1 group once. Each time: parm = parms[i]
@everywhere function single_prob(hop,u_int,mu,N,M,bss,MB_state,time,ini_st,hbar)
    # -----          Hamiltonian:  H2D_kin(hop , N , M , hbt, bss)        ---------------
    hbt=binomial(M+N-1,M-1);
    H1 = zeros(Float64,hbt,hbt) # initialize the kinetic hamiltonian
    Mhalf = convert(Int, M/2)   # length of the lattice = 4.
    for v in 1: hbt             # for each basis state |v>, we calculate one column in H_kin
        for j in 1:M           # consider j-th site of the given basis |v>
            if bss[v][j]>0     # if the j-th site is not empty, then run the following.
                if j != 1&& j !=(M/2+1)               #  hop to left <-
                    vector1= copy(bss[v])            #  initialize the state.
                    vector1[j]= vector1[j]-1         #   a_j annihilate 1 particle at site j
                    vector1[j-1]= vector1[j-1]+1     #    a†i create 1 particle at site i=j-1
                    idx1 = findall(x->x==vector1, bss) # find the index of new basis |vector1>
                    H1[idx1[1],v]=copy( -hop[j,j-1]*sqrt((bss[v][j-1]+1)*bss[v][j]) )# coppresponding matrix element value
                end
                if j != M/2 && j != M              # hop to right ->
                    vector2= copy(bss[v]) 
                    vector2[j]= vector2[j]-1       # a_j annihilate 1 particle at site j
                    vector2[j+1]= vector2[j+1]+1   # a†i create 1 particle at site j+1
                    idx2 = findall(x->x==vector2, bss) # find the index of new basis |vector2>
                    H1[idx2[1],v] = copy(- hop[j,j+1] *sqrt((bss[v][j+1]+1)*bss[v][j]))
                end
                if j in 1:M/2                     # hop to bottom from site j to j+M/2
                    vector3 = copy(bss[v]) 
                    vector3[j]= vector3[j]-1      #   a_j annihilate 1 particle at site j
                    vector3[j+Mhalf]= vector3[j+Mhalf]+1  #    a†i create 1 particle at site i=j+M/2
                    idx3 = findall(x->x==vector3, bss) # find the index of new basis |vector1>
                    H1[idx3[1],v] = copy(-(hop[j,j+Mhalf])*sqrt((bss[v][j+Mhalf]+1)*bss[v][j]) )# assign the coppresponding matrix element values
                end
                if j in (M/2+1):M                # hop to top from site j to j-M/2
                    vector4 = copy(bss[v]) 
                    vector4[j]= vector4[j]-1      #   a_j annihilate 1 particle at site j
                    vector4[j-Mhalf]= vector4[j-Mhalf]+1  #    a†i create 1 particle at site i=j+M/2
                    idx4 = findall(x->x==vector4, bss) # find the index of new basis |vector1>
                    H1[idx4[1],v] = copy( -(hop[j,j-Mhalf])*sqrt((bss[v][j-Mhalf]+1)*bss[v][j])) # assign the coppresponding matrix element values
                end                
            end
        end
    end
#     H1
    # ------------------ H2D_int( u_int,N,M,hbt, bss) ------------------
    H2=zeros(Float64,hbt,hbt)
    for v in 1:hbt
        sum1=sum( (u_int[i]/2)*bss[v][i]*(bss[v][i]-1) for i in 1:M) 
        H2[v,v] = sum1 
    end
#     H2
    # ------------------    H2D_chem( mu,N,M,hbt, bss)   ------------------
    H3=zeros(Float64,hbt,hbt)
    for v in 1:hbt
        sum2 = sum( mu[i]*bss[v][i] for i in 1:M) 
        H3[v,v] = sum2
    end
#     H3
    H_total = H1+H2+H3    
    #---------------  all the eigen values and eigen vectors.   ----------------
    eig_vals1, eig_vecs1 = eigen(H_total)
    #------------------\Psi_{E_x}><Psi_{E_x}\  basis transition for x-th eigenstate, x in hbt.---------------
    basis_trans_bys1 = []
    for x in 1: hbt
        basis_trans = eig_vecs1[: ,x] * reshape(eig_vecs1[:,x],1,:)
        push!( basis_trans_bys1 , basis_trans)
    end
    #------------------  state_evolution_bys1    ---------------
    #     MB_state[ini_st] is the  manybody state of chosen initial state, MB_state[2] = 02000000...
    seb = sum( exp(-1*im*eig_vals1[i]*(time/hbar))*basis_trans_bys1[i]*MB_state[ini_st]  for i in 1:hbt)
    #------------------    prob_total, prob_sort_total, prob_accum_total   ---------------
    prob = [(abs(dot(MB_state[i],seb)))^2 for i in 1:hbt]  # prob of occurrence of each Fock basis in the current state after evolution
    return prob
end

function full_prob_j(;u_full,mu_full, parm , N , M , bss ,MB_state,  time , ini_st, hbar) # inside this function we run multi-processing
    hbt = binomial(M+N-1,M-1);
    p_n = length(parm);  # number of parameter configuration candidates
    hop_tot = parm;      ##################
    # pmap: multi-processing
    prob_total = pmap(single_prob, hop_tot,[u_full for i in 1:p_n],[mu_full for i in 1:p_n],[N for i in 1:p_n],[M for i in 1:p_n],[bss for i in 1:p_n],[MB_state for i in 1:p_n],[t_evl for i in 1:p_n],[ini_st for i in 1:p_n],[hbar for i in 1:p_n])
    return   prob_total
end

function full_prob_u(;j_full, mu_full, parm , N , M , bss ,MB_state,  time , ini_st, hbar) # inside this function we run multi-processing
    hbt = binomial(M+N-1,M-1);
    p_n = length(parm); # number of parameter configuration candidates: 9^5 for 1-st group
    u_int = parm; ##################
    # pmap: multi-processing
    prob_total = pmap(single_prob, [j_full for i in 1:p_n],u_int,[mu_full for i in 1:p_n],[N for i in 1:p_n],[M for i in 1:p_n],[bss for i in 1:p_n],[MB_state for i in 1:p_n],[t_evl for i in 1:p_n],[ini_st for i in 1:p_n],[hbar for i in 1:p_n])
    return   prob_total
end

##################################################################################
### Function 4:  generate the measurement by the correct parameter we assign. it contains mt number of measurements.
# function measurement(;prob_c, mt)
#     hbt = length(prob_c)
#     prob_sort = sort(prob_c , rev=true);
#     prob_accum = [ sum( prob_sort[j] for j in 1:p)  for p in 1:hbt]
#     rnds = rand(mt);
#     measure = zeros(Int64,mt)
#     for i in 1:mt                   
#         prob_copy = copy(prob_accum);
#         push!(prob_copy, rnds[i])             
#         sort!(prob_copy)                      
#         idx = findall(x->x== rnds[i], prob_copy )[1]      
#         measure[i] = findall(x->x== prob_sort[idx] , prob_c)[1]
#     end
#     return measure
# end

#################################################################################
### Function 5:    Bayesian inference (maximumm likelihood estimation) with Logarithm likelihood function
function bayesian_log(; full_probs, measure )
    p_n = length(full_probs); 
    mt = length(measure);
    out = [( sum( log(full_probs[l][measure[i]])  for  i in 1:mt))  for l in 1:p_n] 
    return out
end

#################################################################################
### Function 6:    Bayesian inference (maximumm likelihood estimation) with Logarithm likelihood function
function mle_update_j(;gn, precise,j_full,u_full,mu_full,j_boxes,N,M,bss,MB_state,time=t_evl, ini_st = ini_st,hbar,measure_c=measure_c,mt=mt,j_shares) 
    measure_set= length(measure_c);
    parm = full_parm_j(precise = precise, gn = gn ,j_full=j_full,j_boxes=j_boxes,shares=j_shares);
    full_probs = full_prob_j(u_full = u_full, mu_full = mu_full, parm=parm, N=N,M=M,bss=bss,MB_state=MB_state,time=t_evl,ini_st=ini_st, hbar=hbar ); 
    out = [];
    for ii in 1:measure_set
        posterior = bayesian_log(full_probs = full_probs, measure = measure_c[ii] );
        push!(out, argmax(posterior))
    end
    y = out;  u=unique(y); #count the occurence of each parameter.
    d=Dict([(i,count(x->x==i,y)) for i in u])
    newupdate = argmax(d);
    println("Group index = $gn . ")
    j_chosen = parm[newupdate];
    
    group = j_boxes[gn];
    
    j_full[ group[1][1], group[1][2]] = j_full[ group[1][2], group[1][1]] = copy(j_chosen[ group[1][1], group[1][2]])
    j_full[ group[2][1], group[2][2]] = j_full[ group[2][2], group[2][1]] = copy(j_chosen[ group[2][1], group[2][2]])
    j_full[ group[3][1], group[3][2]] = j_full[ group[3][2], group[3][1]] = copy(j_chosen[ group[3][1], group[3][2]])
    j_full[ group[4][1], group[4][2]] = j_full[ group[4][2], group[4][1]] = copy(j_chosen[ group[4][1], group[4][2]])

    println("j_full = ",j_full)
    return j_full
end


function mle_update_u(;gn, precise,j_full,u_full,mu_full,u_groups,N,M,bss,MB_state,time=t_evl, ini_st = ini_st,hbar,measure_c=measure_c,mt=mt,u_shares) 
    measure_set= length(measure_c);
    parm = full_parm_u(u_full = u_full, u_groups = u_groups, precise=precise, gn=gn, shares=u_shares )
    full_probs = full_prob_u( j_full=j_full, mu_full=mu_full, parm=parm, N=N,M=M,bss=bss,MB_state=MB_state,time=t_evl,ini_st=ini_st, hbar=hbar ); 
    out = [];
    for ii in 1:measure_set
        posterior = bayesian_log(full_probs = full_probs, measure = measure_c[ii] );
        push!(out, argmax(posterior))
    end
    y = out;  u=unique(y); #count the occurence of each parameter.
    d=Dict([(i,count(x->x==i,y)) for i in u])
    newupdate = argmax(d);  # the chosen parm configuration is the newupdate-th in variable parm
    u_chosen = parm[newupdate];

    group = u_groups[gn];
    for j in group
        println("\n Update u_full[$j] from $(u_full[j]) -> $(u_chosen[j])")
        u_full[j] = copy(u_chosen[j]);
    end
    return u_full
end



### Function 7: Define the correct parameter by hand

function parm_initial(; M, j_boxes, j_midval,u_midval,mu_midval)
    jout = zeros(Float64,M,M);
    uout = ones(Float64,M).*u_midval;
    muout = ones(Float64,M).*mu_midval;
    parmleng = length(j_boxes);
    for i in 1:parmleng
        group = j_boxes[i]
        for j in group
           jout[j[1],j[2]] = jout[j[2],j[1]] = j_midval;
        end
    end
    return jout, uout, muout
end

j_midval = 1;  u_midval = 2;  mu_midval = 1;

# -------------  initial parms:  all the estimation update will be push in to these arrays  ----------------------
j_full , u_full, mu_full = parm_initial(M=M, j_boxes=j_boxes, j_midval=j_midval,u_midval=u_midval,mu_midval=mu_midval)

###       Read the measurements      ####

shots_all = readdlm("comparison_50_sets_set$(set_num)_shots.txt", Int64)
measure_c = [shots_all[i,:] for i in 1:size(shots_all)[1]];
println("length of these shots is ",length(shots_all))




#################################################
####                                        #####
####              estimate J/U              #####
####                                        #####
#################################################


for run_num in 1:tot_run_num
    println("run number is $(run_num)")
    
    ###   estimate J   ###
    record_j_full = [];
    
    open("mydata_J_Set$set_num-run$run_num.txt", "a") do io
    write(io, "\n \n The initial guess is:\n ($j_full, $u_full , $mu_full)  " )
    end;
    
    for iiii in 1:iterations
        for iii in 1:length(j_boxes)
            update = mle_update_j(gn=iii,precise=precision[iiii],j_full=j_full,u_full=u_full,mu_full=mu_full,j_boxes=j_boxes,N=N,M=M,bss=bss,MB_state=MB_state,time=t_evl,ini_st=ini_st,hbar=hbar,measure_c=measure_c,mt=mt,j_shares=j_shares)
            open("mydata_J_Set$set_num-run$run_num.txt", "a") do io
                write(io, " \n For $iiii -th iteration, $iii -th group ,the j_full are: \n  $j_full " )
            end;
            push!(record_j_full,j_full)
        end
    end
    open("mydata_J_Set$set_num-run$run_num.txt", "a") do io
        write(io, "\n \n Final J_full :\n ($j_full , $u_full , $mu_full) " )
    end;    
    writedlm("record_J_Set$set_num-run$run_num.txt", record_j_full)
    
    ###   estimate U   ###
    record_u_full = [];

    open("mydata_U_Set$set_num-run$run_num.txt", "a") do io
    write(io, "\n \n The initial guess is:\n ($j_full, $u_full , $mu_full)  " )
    end;
    
    for iiii in 1:iterations 
        for iii in 1:length(u_groups)
            update = mle_update_u(gn=iii,precise=precision[iiii],j_full=j_full,u_full=u_full,mu_full=mu_full,u_groups=u_groups,N=N,M=M,bss=bss,MB_state=MB_state,time=t_evl,ini_st=ini_st,hbar=hbar,measure_c=measure_c,mt=mt,u_shares=u_shares)
            open("mydata_U_Set$set_num-run$run_num.txt", "a") do io
                write(io, "\n For $iiii -th iteration, $iii -th group ,the updates are:\n $update" )
            end;
            push!(record_u_full,u_full)

        end
    end
    open("mydata_U_Set$set_num-run$run_num.txt", "a") do io
        write(io, "\n \n Final u_full:\n $u_full " )
    end;

    writedlm("record_U_Set$set_num-run$run_num.txt", record_u_full)
    
end

writedlm("final_U_Set$set_num.txt", u_full)
writedlm("final_J_Set$set_num.txt", j_full)