"""
Author: Guliuxin Jin, Agnes Valenti
Date: 01.03.2021

This code generates 'mt' projective measurement snapshots from a time-evolution under a
Bose-Hubbard Hamiltonian with randomly generated parameters within the intervals
J_ij in [0.995,1.005]
U_i in [1.99,2.01]
mu_i in [0.995,1.005]
"""

#number of snapshots
mt = 2500; 


# detect if using SLURM
#const IN_SLURM = "SLURM_JOBID" in keys(ENV)

# load packages
using Distributed
#IN_SLURM && using ClusterManagers

# Here we create our parallel julia processes
#if IN_SLURM
#    pids = addprocs_slurm(parse(Int, ENV["SLURM_NTASKS"]))
#    print("\n")
#else
#    pids = addprocs()
#end


@everywhere using LinearAlgebra, Random, DelimitedFiles

###################################################################
##################                             ####################
##################     Declare  parameters     ####################
##################                             ####################
###################################################################

hbar = 1 ;  N = 4 ;   M = 8 ;  ###  N: number of atoms, M: number of lattice sites

#t_evl: time after that measurement snapshots are taken
t_evl=200;

#20 data sets
timesteps= 1; 

ini_st = 150;  # initial state = Fock state No.150  time evolution:200*hbar

#number of measurement sets
number_measurements=20
measure_set= 1; 


parm_order = (((1),(5,2)), ((5),(6,1)), ((6),(2,5,7)), ((2),(1,6,3)), ((3),(2,7,4)), ((7),(3,6,8)), ((8),(7,4)), ((4),(8,3)) );


#parameter intervals
j_midval = 1;  u_midval = 2;  mu_midval = 1;

# hopping amplitudes: distributed region [0, 0.01) + 0.995 = [0.995, 1.005)
j_rand() =  rand().*0.01 .+(0.995*j_midval) 

# on-site repulsions: distributed region [0, 0.2) + 1.99 = [1.99, 2.01)
u_rand() =  rand().*0.02 .+(1.99) 

#chemical potentials:
mu_rand() = rand().*0.01 .+(0.995*mu_midval)


print("Generate measurement snapshots... (2500 shots)")


function basis(N,M)  
    """Creates an array assigning an index to each Fock state for N particles in M lattice sites"""
    
    #Hilbert space dimension
    hbt=binomial(M+N-1,M-1) 

    #bss_outcome[i]: atom configuration of Fock state with index i
    bss_outcome = []
    a = zeros(Int64,M)
    for i in 1:(N+1)^M
        if sum(a)==N
            b=copy(a)
            push!(bss_outcome,b)
        end        
        a[M]+=1
        for j in 0:M-2
            if a[M-j]==N+1
                a[M-j]=0
                a[M-j-1] +=1
            end 
        end
    end
    bss_outcome

    bsslenth = length(bss_outcome) 
    fullmb = []
    for s in 1:bsslenth
        mbstate = zeros(Int64, hbt)
        mbstate[s]=1
        push!(fullmb,mbstate)
    end
    return bss_outcome, fullmb
end
bss, MB_state = basis(N,M); 




@everywhere function single_prob(hop,u_int,mu,N,M,bss,MB_state,time,ini_st,hbar)
    """returns a vector p, where p[i] corresponds to the probability of obtaining Fock state 'i' after a projective measurement on the system, which has been time-evolved for 'time' from the initial state 'ini_st'. The Bose-Hubbard Hamiltonian is specified by the hopping amplitudes ('hop'), on-site repulsions ('u_int') and chemical potentials ('mu')"""
    # -----          Hamiltonian:  H2D_kin(hop , N , M , hbt, bss)        ---------------
    hbt=binomial(M+N-1,M-1);
    
    # initialize the kinetic hamiltonian
    H1 = zeros(Float64,hbt,hbt) 
    Mhalf = convert(Int, M/2)   

    # for each basis state |v>, we calculate one column in H_kin
    for v in 1: hbt       
        # consider j-th site of the given basis |v>    
        for j in 1:M           
            # if the j-th site is not empty, then run the following
            if bss[v][j]>0     
                #  hopping to the left <-
                if j != 1&& j !=(M/2+1)      
                    #  initialize the state
                    vector1= copy(bss[v])            
                    #   a_j annihilate 1 particle at site j
                    vector1[j]= vector1[j]-1         
                    #    a†i create 1 particle at site i=j-1
                    vector1[j-1]= vector1[j-1]+1     
                    # find the index of new basis |vector1>
                    idx1 = findall(x->x==vector1, bss) 
                    # coppresponding matrix element value
                    H1[idx1[1],v]=copy( -hop[j,j-1]*sqrt((bss[v][j-1]+1)*bss[v][j]) )
                end

                # hopping to the right ->
                if j != M/2 && j != M             
                    vector2= copy(bss[v]) 
                    # a_j annihilate 1 particle at site j
                    vector2[j]= vector2[j]-1  
                    # a†i create 1 particle at site j+1
                    vector2[j+1]= vector2[j+1]+1   
                    # find the index of new basis |vector2>
                    idx2 = findall(x->x==vector2, bss) 
                    H1[idx2[1],v] = copy(- hop[j,j+1] *sqrt((bss[v][j+1]+1)*bss[v][j]))
                end
           
                # hopping to the bottom from site j to j+M/2
                if j in 1:M/2                     
                    vector3 = copy(bss[v]) 
                    #   a_j annihilate 1 particle at site j
                    vector3[j]= vector3[j]-1      
                    #    a†i create 1 particle at site i=j+M/2
                    vector3[j+Mhalf]= vector3[j+Mhalf]+1  
                    # find the index of new basis |vector1>
                    idx3 = findall(x->x==vector3, bss) 
                    # assign the coppresponding matrix element values
                    H1[idx3[1],v] = copy(-(hop[j,j+Mhalf])*sqrt((bss[v][j+Mhalf]+1)*bss[v][j]) )
                end

                # hopping to the top from site j to j-M/2
                if j in (M/2+1):M                
                    vector4 = copy(bss[v]) 
                    #   a_j annihilate 1 particle at site j
                    vector4[j]= vector4[j]-1      
                    #    a†i create 1 particle at site i=j+M/2
                    vector4[j-Mhalf]= vector4[j-Mhalf]+1  
                    # find the index of new basis |vector1>
                    idx4 = findall(x->x==vector4, bss) 
                    # assign the coppresponding matrix element values
                    H1[idx4[1],v] = copy( -(hop[j,j-Mhalf])*sqrt((bss[v][j-Mhalf]+1)*bss[v][j])) 
                end                
            end
        end
    end
    H1

    # ------------------ H2D_int( u_int,N,M,hbt, bss) ------------------
    H2=zeros(Float64,hbt,hbt)
    for v in 1:hbt
        sum1=sum( (u_int[i]/2)*bss[v][i]*(bss[v][i]-1) for i in 1:M) 
        H2[v,v] = sum1 
    end
    H2

    # ------------------    H2D_chem( mu,N,M,hbt, bss)   ------------------
    H3=zeros(Float64,hbt,hbt)
    for v in 1:hbt
        sum2 = sum( mu[i]*bss[v][i] for i in 1:M) 
        H3[v,v] = sum2
    end
    H3
    H_total = H1+H2-H3    
    #---------------  all the eigen values and eigen vectors.   ----------------
    eig_vals1, eig_vecs1 = eigen(H_total)   

    #------------------\Psi_{E_x}><Psi_{E_x}\  basis transition for x-th eigenstate, x in hbt.---------------
    basis_trans_bys1 = []
    for x in 1: hbt
        basis_trans = eig_vecs1[: ,x] * reshape(eig_vecs1[:,x],1,:)  
        push!( basis_trans_bys1 , basis_trans)     
    end

    #------------------  state_evolution_bys1    ---------------
    #     MB_state[ini_st] is the  manybody state of chosen initial state, MB_state[2] = 02000000...
    seb = sum( exp(-1*im*eig_vals1[i]*(time/hbar))*basis_trans_bys1[i]*MB_state[ini_st]  for i in 1:hbt)

    #------------------    prob_total, prob_sort_total, prob_accum_total   ---------------
    # probability of occurrence of each Fock basis in the current state after evolution
    prob = [(abs(dot(MB_state[i],seb)))^2 for i in 1:hbt]  
    return prob
end


function measurement(;prob_c, mt)
    """generates mt snapshots given the probabilities prob_c[i] for Fock state i to be obtained. The respective snapshot is saved via its Fock state index"""
    hbt = length(prob_c)
    prob_sort = sort(prob_c , rev=true);
    prob_accum = [ sum( prob_sort[j] for j in 1:p)  for p in 1:hbt]
    rnds = rand(mt);
    measure = zeros(Int64,mt)
    for i in 1:mt                   
            prob_copy = copy(prob_accum);
            push!(prob_copy, rnds[i])             
            sort!(prob_copy)                      
            idx = findall(x->x== rnds[i], prob_copy )[1]      
            measure[i] = findall(x->x== prob_sort[idx] , prob_c)[1]
    end
    return measure
end


function parm_correct(;M, parm_order)  
    """generates a set of correct parameters"""
    jout = zeros(Float64,M,M);
    uout = zeros(Float64,M);
    muout = zeros(Float64,M);

    parmleng = length(parm_order);
    for i in 1:parmleng
        group = parm_order[i]
        center = parm_order[i][1]     
        neighbour = parm_order[i][2]  
        for j in neighbour
            jout[center,j] = jout[j,center] = j_rand() 
        end
        uout[center] = u_rand();
        muout[center] = mu_rand()
    end
    return jout, uout, muout
end



function data_gen(;num_parms)
    """Generates 'num_parms' parameter sets randomly within the specified intervals and the respective measurement snapshots"""
    j_c_total = []; u_c_total = []; mu_c_total = [];
    for i in 1: num_parms
        j_c,u_c,mu_c = parm_correct(M=M, parm_order=parm_order)
       
        push!(j_c_total,j_c)
        push!(u_c_total, u_c)
        push!(mu_c_total, mu_c)
    end
    prob_c_total = pmap(single_prob, j_c_total, u_c_total ,mu_c_total,[N for i in 1:num_parms],[M for i in 1:num_parms],[bss for i in 1:num_parms],[MB_state for i in 1:num_parms],[t_evl for i in 1:num_parms],[ini_st for i in 1:num_parms],[hbar for i in 1:num_parms])
    
    measure_c_total = []
    for i in 1:num_parms
        prob_c = prob_c_total[i]
        measure_c = [];
        measure_c = [ measurement(;prob_c = prob_c, mt = mt)    for i in 1:measure_set];
        push!(measure_c_total, measure_c)
    end
    return j_c_total, u_c_total, mu_c_total, measure_c_total
end

j12_labels=zeros(Float64,number_measurements)
j15_labels=zeros(Float64,number_measurements)
j23_labels=zeros(Float64,number_measurements)
j26_labels=zeros(Float64,number_measurements)
j34_labels=zeros(Float64,number_measurements)
j37_labels=zeros(Float64,number_measurements)
j48_labels=zeros(Float64,number_measurements)
j56_labels=zeros(Float64,number_measurements)
j67_labels=zeros(Float64,number_measurements)
j78_labels=zeros(Float64,number_measurements)
u1_labels=zeros(Float64,number_measurements)
u2_labels=zeros(Float64,number_measurements)
u3_labels=zeros(Float64,number_measurements)
u4_labels=zeros(Float64,number_measurements)
u5_labels=zeros(Float64,number_measurements)
u6_labels=zeros(Float64,number_measurements)
u7_labels=zeros(Float64,number_measurements)
u8_labels=zeros(Float64,number_measurements)
mu2diff_labels=zeros(Float64,number_measurements)
mu3diff_labels=zeros(Float64,number_measurements)
mu4diff_labels=zeros(Float64,number_measurements)
mu5diff_labels=zeros(Float64,number_measurements)
mu6diff_labels=zeros(Float64,number_measurements)
mu7diff_labels=zeros(Float64,number_measurements)
mu8diff_labels=zeros(Float64,number_measurements)

###       Generate the measurements based on the above correct parameters      ####
for i in 1:number_measurements
  #@time begin
    #print(i,"\n")
    j_t,u_t,mu_t,measures = data_gen(num_parms=timesteps);
    measure_loop=measures[1]
    writedlm("data_2p5k_new/snapshots$i.txt", measure_loop)
  #end
  #save shots
    j_loop=j_t[1]
    j12_labels[i]=j_loop[1,2]
    j15_labels[i]=j_loop[1,5]
    j23_labels[i]=j_loop[2,3]
    j26_labels[i]=j_loop[2,6]
    j34_labels[i]=j_loop[3,4]
    j37_labels[i]=j_loop[3,7]
    j48_labels[i]=j_loop[4,8]
    j56_labels[i]=j_loop[5,6]
    j67_labels[i]=j_loop[6,7]
    j78_labels[i]=j_loop[7,8]

    u_loop=u_t[1]
    u1_labels[i]=u_loop[1]
    u2_labels[i]=u_loop[2]
    u3_labels[i]=u_loop[3]
    u4_labels[i]=u_loop[4]
    u5_labels[i]=u_loop[5]
    u6_labels[i]=u_loop[6]
    u7_labels[i]=u_loop[7]
    u8_labels[i]=u_loop[8]

    mu_loop=mu_t[1]
    mu2diff_labels[i]=mu_loop[2]-mu_loop[1]
    mu3diff_labels[i]=mu_loop[3]-mu_loop[1]
    mu4diff_labels[i]=mu_loop[4]-mu_loop[1]
    mu5diff_labels[i]=mu_loop[5]-mu_loop[1]
    mu6diff_labels[i]=mu_loop[6]-mu_loop[1]
    mu7diff_labels[i]=mu_loop[7]-mu_loop[1]
    mu8diff_labels[i]=mu_loop[8]-mu_loop[1]
end

print("\n","Snapshots generated.","\n","\n")

#print(j_t[1])
#write the generated data into files
writedlm("data_2p5k_new/eval_labels_j12.txt", j12_labels)
writedlm("data_2p5k_new/eval_labels_j15.txt", j15_labels)
writedlm("data_2p5k_new/eval_labels_j23.txt", j23_labels)
writedlm("data_2p5k_new/eval_labels_j26.txt", j26_labels)
writedlm("data_2p5k_new/eval_labels_j34.txt", j34_labels)
writedlm("data_2p5k_new/eval_labels_j37.txt", j37_labels)
writedlm("data_2p5k_new/eval_labels_j48.txt", j48_labels)
writedlm("data_2p5k_new/eval_labels_j56.txt", j56_labels)
writedlm("data_2p5k_new/eval_labels_j67.txt", j67_labels)
writedlm("data_2p5k_new/eval_labels_j78.txt", j78_labels)

writedlm("data_2p5k_new/eval_labels_u1.txt", u1_labels)
writedlm("data_2p5k_new/eval_labels_u2.txt", u2_labels)
writedlm("data_2p5k_new/eval_labels_u3.txt", u3_labels)
writedlm("data_2p5k_new/eval_labels_u4.txt", u4_labels)
writedlm("data_2p5k_new/eval_labels_u5.txt", u5_labels)
writedlm("data_2p5k_new/eval_labels_u6.txt", u6_labels)
writedlm("data_2p5k_new/eval_labels_u7.txt", u7_labels)
writedlm("data_2p5k_new/eval_labels_u8.txt", u8_labels)

writedlm("data_2p5k_new/eval_labels_mu2diff.txt", mu2diff_labels)
writedlm("data_2p5k_new/eval_labels_mu3diff.txt", mu3diff_labels)
writedlm("data_2p5k_new/eval_labels_mu4diff.txt", mu4diff_labels)
writedlm("data_2p5k_new/eval_labels_mu5diff.txt", mu5diff_labels)
writedlm("data_2p5k_new/eval_labels_mu6diff.txt", mu6diff_labels)
writedlm("data_2p5k_new/eval_labels_mu7diff.txt", mu7diff_labels)
writedlm("data_2p5k_new/eval_labels_mu8diff.txt", mu8diff_labels)



