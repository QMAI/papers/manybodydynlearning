import numpy as np
import matplotlib.pyplot as plt


j_bayes1=np.loadtxt("final_J_Set1.txt")
j_bayes2=np.loadtxt("final_J_Set2.txt")
j_bayes3=np.loadtxt("final_J_Set3.txt")
j_bayes4=np.loadtxt("final_J_Set4.txt")
j_bayes5=np.loadtxt("final_J_Set5.txt")
j_bayes6=np.loadtxt("final_J_Set6.txt")
j_bayes7=np.loadtxt("final_J_Set7.txt")
j_bayes8=np.loadtxt("final_J_Set8.txt")
j_bayes9=np.loadtxt("final_J_Set9.txt")
j_bayes10=np.loadtxt("final_J_Set10.txt")
j_bayes11=np.loadtxt("final_J_Set11.txt")
j_bayes12=np.loadtxt("final_J_Set12.txt")
j_bayes13=np.loadtxt("final_J_Set13.txt")
j_bayes14=np.loadtxt("final_J_Set14.txt")
j_bayes15=np.loadtxt("final_J_Set15.txt")
j_bayes16=np.loadtxt("final_J_Set16.txt")
j_bayes17=np.loadtxt("final_J_Set17.txt")
j_bayes18=np.loadtxt("final_J_Set18.txt")
j_bayes19=np.loadtxt("final_J_Set19.txt")
j_bayes20=np.loadtxt("final_J_Set20.txt")

#print(np.shape(j_bayes1))
j_correct1=np.loadtxt("../set1/jvalues1.txt")
j_correct2=np.loadtxt("../set2/jvalues1.txt")
j_correct3=np.loadtxt("../set3/jvalues1.txt")
j_correct4=np.loadtxt("../set4/jvalues1.txt")
j_correct5=np.loadtxt("../set5/jvalues1.txt")
j_correct6=np.loadtxt("../set6/jvalues1.txt")
j_correct7=np.loadtxt("../set7/jvalues1.txt")
j_correct8=np.loadtxt("../set8/jvalues1.txt")
j_correct9=np.loadtxt("../set9/jvalues1.txt")
j_correct10=np.loadtxt("../set10/jvalues1.txt")
j_correct11=np.loadtxt("../set11/jvalues1.txt")
j_correct12=np.loadtxt("../set12/jvalues1.txt")
j_correct13=np.loadtxt("../set13/jvalues1.txt")
j_correct14=np.loadtxt("../set14/jvalues1.txt")
j_correct15=np.loadtxt("../set15/jvalues1.txt")
j_correct16=np.loadtxt("../set16/jvalues1.txt")
j_correct17=np.loadtxt("../set17/jvalues1.txt")
j_correct18=np.loadtxt("../set18/jvalues1.txt")
j_correct19=np.loadtxt("../set19/jvalues1.txt")
j_correct20=np.loadtxt("../set20/jvalues1.txt")


u_bayes1=np.loadtxt("final_U_Set1.txt")
u_bayes2=np.loadtxt("final_U_Set2.txt")
u_bayes3=np.loadtxt("final_U_Set3.txt")
u_bayes4=np.loadtxt("final_U_Set4.txt")
u_bayes5=np.loadtxt("final_U_Set5.txt")
u_bayes6=np.loadtxt("final_U_Set6.txt")
u_bayes7=np.loadtxt("final_U_Set7.txt")
u_bayes8=np.loadtxt("final_U_Set8.txt")
u_bayes9=np.loadtxt("final_U_Set9.txt")
u_bayes10=np.loadtxt("final_U_Set10.txt")
u_bayes11=np.loadtxt("final_U_Set11.txt")
u_bayes12=np.loadtxt("final_U_Set12.txt")
u_bayes13=np.loadtxt("final_U_Set13.txt")
u_bayes14=np.loadtxt("final_U_Set14.txt")
u_bayes15=np.loadtxt("final_U_Set15.txt")
u_bayes16=np.loadtxt("final_U_Set16.txt")
u_bayes17=np.loadtxt("final_U_Set17.txt")
u_bayes18=np.loadtxt("final_U_Set18.txt")
u_bayes19=np.loadtxt("final_U_Set19.txt")
u_bayes20=np.loadtxt("final_U_Set20.txt")

u_correct1=np.loadtxt("../set1/uvalues1.txt")
u_correct2=np.loadtxt("../set2/uvalues1.txt")
u_correct3=np.loadtxt("../set3/uvalues1.txt")
u_correct4=np.loadtxt("../set4/uvalues1.txt")
u_correct5=np.loadtxt("../set5/uvalues1.txt")
u_correct6=np.loadtxt("../set6/uvalues1.txt")
u_correct7=np.loadtxt("../set7/uvalues1.txt")
u_correct8=np.loadtxt("../set8/uvalues1.txt")
u_correct9=np.loadtxt("../set9/uvalues1.txt")
u_correct10=np.loadtxt("../set10/uvalues1.txt")
u_correct11=np.loadtxt("../set11/uvalues1.txt")
u_correct12=np.loadtxt("../set12/uvalues1.txt")
u_correct13=np.loadtxt("../set13/uvalues1.txt")
u_correct14=np.loadtxt("../set14/uvalues1.txt")
u_correct15=np.loadtxt("../set15/uvalues1.txt")
u_correct16=np.loadtxt("../set16/uvalues1.txt")
u_correct17=np.loadtxt("../set17/uvalues1.txt")
u_correct18=np.loadtxt("../set18/uvalues1.txt")
u_correct19=np.loadtxt("../set19/uvalues1.txt")
u_correct20=np.loadtxt("../set20/uvalues1.txt")


indices=np.zeros((10,2),dtype='int')
indices[0,0]=1-1
indices[0,1]=2-1
indices[1,0]=1-1
indices[1,1]=5-1
indices[2,0]=2-1
indices[2,1]=3-1
indices[3,0]=2-1
indices[3,1]=6-1
indices[4,0]=3-1
indices[4,1]=4-1
indices[5,0]=3-1
indices[5,1]=7-1
indices[6,0]=4-1
indices[6,1]=8-1
indices[7,0]=5-1
indices[7,1]=6-1
indices[8,0]=6-1
indices[8,1]=7-1
indices[9,0]=7-1
indices[9,1]=8-1

for i in range(10):
  print(indices[i,0])
  plabels=np.array([j_correct1[indices[i,0],indices[i,1]],j_correct2[indices[i,0],indices[i,1]], j_correct3[indices[i,0],indices[i,1]], j_correct4[indices[i,0],indices[i,1]], j_correct5[indices[i,0],indices[i,1]],   j_correct6[indices[i,0],indices[i,1]],j_correct7[indices[i,0],indices[i,1]], j_correct8[indices[i,0],indices[i,1]], j_correct9[indices[i,0],indices[i,1]], j_correct10[indices[i,0],indices[i,1]],  j_correct11[indices[i,0],indices[i,1]],j_correct12[indices[i,0],indices[i,1]], j_correct13[indices[i,0],indices[i,1]], j_correct14[indices[i,0],indices[i,1]], j_correct15[indices[i,0],indices[i,1]],   j_correct16[indices[i,0],indices[i,1]],j_correct17[indices[i,0],indices[i,1]], j_correct18[indices[i,0],indices[i,1]], j_correct19[indices[i,0],indices[i,1]], j_correct20[indices[i,0],indices[i,1]] ])
  pbetas=np.array([j_bayes1[indices[i,0],indices[i,1]],j_bayes2[indices[i,0],indices[i,1]], j_bayes3[indices[i,0],indices[i,1]], j_bayes4[indices[i,0],indices[i,1]], j_bayes5[indices[i,0],indices[i,1]],   j_bayes6[indices[i,0],indices[i,1]],j_bayes7[indices[i,0],indices[i,1]], j_bayes8[indices[i,0],indices[i,1]], j_bayes9[indices[i,0],indices[i,1]], j_bayes10[indices[i,0],indices[i,1]],  j_bayes11[indices[i,0],indices[i,1]],j_bayes12[indices[i,0],indices[i,1]], j_bayes13[indices[i,0],indices[i,1]], j_bayes14[indices[i,0],indices[i,1]], j_bayes15[indices[i,0],indices[i,1]],   j_bayes16[indices[i,0],indices[i,1]],j_bayes17[indices[i,0],indices[i,1]], j_bayes18[indices[i,0],indices[i,1]], j_bayes19[indices[i,0],indices[i,1]], j_bayes20[indices[i,0],indices[i,1]]])

  mean_abs=(np.sum(np.abs(plabels-pbetas))/np.shape(plabels)[0])
  std_dev=np.sqrt(np.mean(np.abs(np.abs(plabels-pbetas)-mean_abs)*np.abs(np.abs(plabels-pbetas)-mean_abs)  ))
  np.savetxt('paramest_error_j{}.txt'.format(i),np.array([mean_abs,std_dev]))
  np.savetxt('params_j{}.txt'.format(i),np.transpose(np.vstack([plabels,pbetas])))


for i in range(8):
  print(i)
  plabels=np.array([u_correct1[i],u_correct2[i], u_correct3[i], u_correct4[i],u_correct5[i],  u_correct6[i],u_correct7[i], u_correct8[i], u_correct9[i],u_correct10[i], u_correct11[i],u_correct12[i], u_correct13[i], u_correct14[i],u_correct15[i],  u_correct16[i],u_correct17[i], u_correct18[i], u_correct19[i],u_correct20[i]])
  pbetas=np.array([u_bayes1[i],u_bayes2[i], u_bayes3[i], u_bayes4[i], u_bayes5[i], u_bayes6[i],u_bayes7[i], u_bayes8[i], u_bayes9[i], u_bayes10[i],  u_bayes11[i],u_bayes12[i], u_bayes13[i], u_bayes14[i], u_bayes15[i], u_bayes16[i],u_bayes17[i], u_bayes18[i], u_bayes19[i], u_bayes20[i]])
  mean_abs=(np.sum(np.abs(plabels-pbetas))/np.shape(plabels)[0])
  std_dev=np.sqrt(np.mean(np.abs(np.abs(plabels-pbetas)-mean_abs)*np.abs(np.abs(plabels-pbetas)-mean_abs)  ))
  np.savetxt('paramest_error_u{}.txt'.format(i+1),np.array([mean_abs,std_dev]))
  np.savetxt('params_u{}.txt'.format(i+1),np.transpose(np.vstack([plabels,pbetas])))
"""
mean_abs=(np.sum(np.abs(plabels-pbetas))/np.shape(plabels)[0])
#print(np.abs(plabels-pbetas))
    #print(np.abs(np.abs(plabels-pbetas)-mean_abs))
    std_dev=np.sqrt(np.mean(np.abs(np.abs(plabels-pbetas)-mean_abs)*np.abs(np.abs(plabels-pbetas)-mean_abs)  ))
    print(mean_abs, std_dev)
    np.savetxt('paramest_error.txt',np.array([mean_abs,std_dev]))
    np.savetxt('params.txt',np.transpose(np.vstack([plabels,pbetas])))
"""
