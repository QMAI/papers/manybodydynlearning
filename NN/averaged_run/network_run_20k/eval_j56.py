"""
Author: Agnes Valenti
Date: 01.03.2021
"""

from __future__ import division
from __future__ import absolute_import
from __future__ import print_function

import sys
sys.path.append('../../network/')

import matplotlib
matplotlib.use('Agg')

import warnings
warnings.filterwarnings('ignore',category=FutureWarning)

import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np
import network as net




saver=tf.train.Saver()
sess = tf.Session()
sess.run(tf.global_variables_initializer())

##########################################################################################################


if True:
    

    #load density correlator expectation values
    pdata=np.loadtxt('../data_20k_new/data_expvalues.txt')
    #pdata=np.hstack((pdata[:,:100],pdata[:,128:]))

    #load correct values j_12
    plabels=np.loadtxt('../data_20k_new/eval_labels_j56.txt')

    #load pre-trained network
    tf.reset_default_graph()
    saver.restore(sess, '../../network/checkpoints_20000shots/pretrained_j56/-2099900')

    #run the network estimation
    pbetas = sess.run([net.beta_output], feed_dict={net.tf_x: pdata})
    pbetas=np.reshape(pbetas,(np.shape(plabels)))
  

    mean_abs=(np.sum(np.abs(plabels-pbetas))/np.shape(plabels)[0])

    #save estimation error in the file 'paramest_error_j56.txt'
    np.savetxt('../network_eval_20k/paramest_error_j56.txt',np.array([mean_abs]))

    #save [correct value, estimated value] in the file 'params_j56.txt'
    np.savetxt('../network_eval_20k/params_j56.txt',np.transpose(np.vstack([plabels,pbetas])))

