
julia generate_snapshots_2p5k.jl
julia convert_to_expvalues_2p5k.jl

echo ' '
echo 'Estimate Bose-Hubbard parameters...'
echo ' '

cd network_run_2p5k
python3 eval_j12.py
python3 eval_j15.py
python3 eval_j23.py
python3 eval_j26.py
python3 eval_j34.py
python3 eval_j37.py
python3 eval_j48.py
python3 eval_j56.py
python3 eval_j67.py
python3 eval_j78.py
python3 eval_u1.py
python3 eval_u2.py
python3 eval_u3.py
python3 eval_u4.py
python3 eval_u5.py
python3 eval_u6.py
python3 eval_u7.py
python3 eval_u8.py
python3 eval_mu2diff.py
python3 eval_mu3diff.py
python3 eval_mu4diff.py
python3 eval_mu5diff.py
python3 eval_mu6diff.py
python3 eval_mu7diff.py
python3 eval_mu8diff.py
cd ../

echo ' '
echo 'Parameters estimated.'
echo ' '

julia generate_snapshots_20k.jl
julia convert_to_expvalues_20k.jl

cd network_run_20k
python3 eval_j12.py
python3 eval_j15.py
python3 eval_j23.py
python3 eval_j26.py
python3 eval_j34.py
python3 eval_j37.py
python3 eval_j48.py
python3 eval_j56.py
python3 eval_j67.py
python3 eval_j78.py
python3 eval_u1.py
python3 eval_u2.py
python3 eval_u3.py
python3 eval_u4.py
python3 eval_u5.py
python3 eval_u6.py
python3 eval_u7.py
python3 eval_u8.py
python3 eval_mu2diff.py
python3 eval_mu3diff.py
python3 eval_mu4diff.py
python3 eval_mu5diff.py
python3 eval_mu6diff.py
python3 eval_mu7diff.py
python3 eval_mu8diff.py
cd ../

echo ' '
echo 'Parameters estimated.'
echo ' '

python3 plot.py
