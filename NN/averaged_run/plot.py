#!/usr/bin/env python
# coding: utf-8

# In[8]:


import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

fig_size_dim    = 8
golden_ratio    = (1+np.sqrt(5))/2
fig_size        = (fig_size_dim, fig_size_dim/golden_ratio)

def plot_style():
    font_size       = 8
    dpi             =  500

    params = {'figure.figsize': fig_size,
              'figure.dpi': dpi,
              'savefig.dpi': dpi,
              'font.size': font_size,
              'font.family': "Tahoma",
              'figure.titlesize': font_size,
              'legend.fontsize': font_size,
              'axes.labelsize': font_size,
              'axes.titlesize': font_size,
              'xtick.labelsize': font_size,
              'ytick.labelsize': font_size,
                }

    plt.rcParams.update(params)
plot_style()


# In[9]:


folder = 'bayes_eval_2p5k/'
#load data for J
params_j12=np.loadtxt(folder+"params_j12.txt")
params_j15=np.loadtxt(folder+"params_j15.txt")
params_j23=np.loadtxt(folder+"params_j23.txt")
params_j26=np.loadtxt(folder+"params_j26.txt")
params_j34=np.loadtxt(folder+"params_j34.txt")
params_j37=np.loadtxt(folder+"params_j37.txt")
params_j48=np.loadtxt(folder+"params_j48.txt")
params_j56=np.loadtxt(folder+"params_j56.txt")
params_j67=np.loadtxt(folder+"params_j67.txt")
params_j78=np.loadtxt(folder+"params_j78.txt")
params_j_correct=np.hstack([params_j12[:,0], params_j15[:,0], params_j23[:,0], params_j26[:,0], params_j34[:,0], params_j37[:,0], params_j48[:,0], params_j56[:,0], params_j67[:,0], params_j78[:,0]])
params_j_est=np.hstack([params_j12[:,1], params_j15[:,1], params_j23[:,1], params_j26[:,1], params_j34[:,1], params_j37[:,1], params_j48[:,1], params_j56[:,1], params_j67[:,1], params_j78[:,1]])
params_j_diff_2k5=params_j_est-params_j_correct
std_dev_j_2k5=np.sqrt(np.mean(params_j_diff_2k5*params_j_diff_2k5)-np.mean(params_j_diff_2k5)**2)
#print(std_dev_j_2k5)
mean_error_J_2k5=np.mean(np.abs(params_j_est-params_j_correct))
#print(mean_error_J_2k5)
#nJ=np.size(params_j_diff)
params_j_diff_2k5*=1000
std_dev_j_2k5*=1000
mean_error_J_2k5*=1000

#load data for U
params_u1=np.loadtxt(folder+"params_u1.txt")
params_u2=np.loadtxt(folder+"params_u2.txt")
params_u3=np.loadtxt(folder+"params_u3.txt")
params_u4=np.loadtxt(folder+"params_u4.txt")
params_u5=np.loadtxt(folder+"params_u5.txt")
params_u6=np.loadtxt(folder+"params_u6.txt")
params_u7=np.loadtxt(folder+"params_u7.txt")
params_u8=np.loadtxt(folder+"params_u8.txt")
params_u_correct=np.hstack([params_u1[:,0], params_u2[:,0], params_u3[:,0], params_u4[:,0], params_u5[:,0], params_u6[:,0], params_u7[:,0], params_u8[:,0]])
params_u_est=np.hstack([params_u1[:,1], params_u2[:,1], params_u3[:,1], params_u4[:,1], params_u5[:,1], params_u6[:,1], params_u7[:,1], params_u8[:,1]])
params_u_diff_2k5=params_u_est-params_u_correct
std_dev_u_2k5=np.sqrt(np.mean(params_u_diff_2k5*params_u_diff_2k5)-np.mean(params_u_diff_2k5)**2)
#print(std_dev_u_2k5)
mean_error_U_2k5=np.mean(np.abs(params_u_est-params_u_correct))
#print(mean_error_U_2k5)
#nU=np.size(params_u_diff)


folder = 'network_eval_2p5k/'
#load data for J
params_j12=np.loadtxt(folder+"params_j12.txt")
params_j15=np.loadtxt(folder+"params_j15.txt")
params_j23=np.loadtxt(folder+"params_j23.txt")
params_j26=np.loadtxt(folder+"params_j26.txt")
params_j34=np.loadtxt(folder+"params_j34.txt")
params_j37=np.loadtxt(folder+"params_j37.txt")
params_j48=np.loadtxt(folder+"params_j48.txt")
params_j56=np.loadtxt(folder+"params_j56.txt")
params_j67=np.loadtxt(folder+"params_j67.txt")
params_j78=np.loadtxt(folder+"params_j78.txt")
params_j_correct_net=np.hstack([params_j12[:,0], params_j15[:,0], params_j23[:,0], params_j26[:,0], params_j34[:,0], params_j37[:,0], params_j48[:,0], params_j56[:,0], params_j67[:,0], params_j78[:,0]])
params_j_est_net=np.hstack([params_j12[:,1], params_j15[:,1], params_j23[:,1], params_j26[:,1], params_j34[:,1], params_j37[:,1], params_j48[:,1], params_j56[:,1], params_j67[:,1], params_j78[:,1]])
params_j_diff_net_2k5=params_j_est_net-params_j_correct_net
std_dev_j_net_2k5=np.sqrt(np.mean(params_j_diff_net_2k5*params_j_diff_net_2k5)-np.mean(params_j_diff_net_2k5)**2)
#print(std_dev_j_net_2k5)
mean_error_J_net_2k5=np.mean(np.abs(params_j_est_net-params_j_correct_net))
#print(mean_error_J_net_2k5)
#nJ=np.size(params_j_diff)
params_j_diff_net_2k5*=1000
std_dev_j_net_2k5*=1000
mean_error_J_net_2k5*=1000

#load data for U
params_u1=np.loadtxt(folder+"params_u1.txt")
params_u2=np.loadtxt(folder+"params_u2.txt")
params_u3=np.loadtxt(folder+"params_u3.txt")
params_u4=np.loadtxt(folder+"params_u4.txt")
params_u5=np.loadtxt(folder+"params_u5.txt")
params_u6=np.loadtxt(folder+"params_u6.txt")
params_u7=np.loadtxt(folder+"params_u7.txt")
params_u8=np.loadtxt(folder+"params_u8.txt")
params_u_correct_net=np.hstack([params_u1[:,0], params_u2[:,0], params_u3[:,0], params_u4[:,0], params_u5[:,0], params_u6[:,0], params_u7[:,0], params_u8[:,0]])
params_u_est_net=np.hstack([params_u1[:,1], params_u2[:,1], params_u3[:,1], params_u4[:,1], params_u5[:,1], params_u6[:,1], params_u7[:,1], params_u8[:,1]])
params_u_diff_net_2k5=params_u_est_net-params_u_correct_net
std_dev_u_net_2k5=np.sqrt(np.mean(params_u_diff_net_2k5*params_u_diff_net_2k5)-np.mean(params_u_diff_net_2k5)**2)
#print(std_dev_u_net_2k5)
mean_error_U_net_2k5=np.mean(np.abs(params_u_est_net-params_u_correct_net))
#print(mean_error_U_net_2k5)
#nU=np.size(params_u_diff)


# In[10]:


folder = 'bayes_eval_20k/'
#load data for J
params_j12=np.loadtxt(folder+"params_j12.txt")
params_j15=np.loadtxt(folder+"params_j15.txt")
params_j23=np.loadtxt(folder+"params_j23.txt")
params_j26=np.loadtxt(folder+"params_j26.txt")
params_j34=np.loadtxt(folder+"params_j34.txt")
params_j37=np.loadtxt(folder+"params_j37.txt")
params_j48=np.loadtxt(folder+"params_j48.txt")
params_j56=np.loadtxt(folder+"params_j56.txt")
params_j67=np.loadtxt(folder+"params_j67.txt")
params_j78=np.loadtxt(folder+"params_j78.txt")
params_j_correct=np.hstack([params_j12[:,0], params_j15[:,0], params_j23[:,0], params_j26[:,0], params_j34[:,0], params_j37[:,0], params_j48[:,0], params_j56[:,0], params_j67[:,0], params_j78[:,0]])
params_j_est=np.hstack([params_j12[:,1], params_j15[:,1], params_j23[:,1], params_j26[:,1], params_j34[:,1], params_j37[:,1], params_j48[:,1], params_j56[:,1], params_j67[:,1], params_j78[:,1]])
params_j_diff=params_j_est-params_j_correct
std_dev_j=np.sqrt(np.mean(params_j_diff*params_j_diff)-np.mean(params_j_diff)**2)
#print(std_dev_j)
mean_error_J=np.mean(np.abs(params_j_est-params_j_correct))
#print(mean_error_J)
#nJ=np.size(params_j_diff)
params_j_diff*=1000
std_dev_j*=1000
mean_error_J*=1000

#load data for U
params_u1=np.loadtxt(folder+"params_u1.txt")
params_u2=np.loadtxt(folder+"params_u2.txt")
params_u3=np.loadtxt(folder+"params_u3.txt")
params_u4=np.loadtxt(folder+"params_u4.txt")
params_u5=np.loadtxt(folder+"params_u5.txt")
params_u6=np.loadtxt(folder+"params_u6.txt")
params_u7=np.loadtxt(folder+"params_u7.txt")
params_u8=np.loadtxt(folder+"params_u8.txt")
params_u_correct=np.hstack([params_u1[:,0], params_u2[:,0], params_u3[:,0], params_u4[:,0], params_u5[:,0], params_u6[:,0], params_u7[:,0], params_u8[:,0]])
params_u_est=np.hstack([params_u1[:,1], params_u2[:,1], params_u3[:,1], params_u4[:,1], params_u5[:,1], params_u6[:,1], params_u7[:,1], params_u8[:,1]])
params_u_diff=params_u_est-params_u_correct
std_dev_u=np.sqrt(np.mean(params_u_diff*params_u_diff)-np.mean(params_u_diff)**2)
#print(std_dev_u)
mean_error_U=np.mean(np.abs(params_u_est-params_u_correct))
#print(mean_error_U)
#nU=np.size(params_u_diff)


folder = 'network_eval_20k/'
#load data for J
params_j12=np.loadtxt(folder+"params_j12.txt")
params_j15=np.loadtxt(folder+"params_j15.txt")
params_j23=np.loadtxt(folder+"params_j23.txt")
params_j26=np.loadtxt(folder+"params_j26.txt")
params_j34=np.loadtxt(folder+"params_j34.txt")
params_j37=np.loadtxt(folder+"params_j37.txt")
params_j48=np.loadtxt(folder+"params_j48.txt")
params_j56=np.loadtxt(folder+"params_j56.txt")
params_j67=np.loadtxt(folder+"params_j67.txt")
params_j78=np.loadtxt(folder+"params_j78.txt")
params_j_correct_net=np.hstack([params_j12[:,0], params_j15[:,0], params_j23[:,0], params_j26[:,0], params_j34[:,0], params_j37[:,0], params_j48[:,0], params_j56[:,0], params_j67[:,0], params_j78[:,0]])
params_j_est_net=np.hstack([params_j12[:,1], params_j15[:,1], params_j23[:,1], params_j26[:,1], params_j34[:,1], params_j37[:,1], params_j48[:,1], params_j56[:,1], params_j67[:,1], params_j78[:,1]])
params_j_diff_net=params_j_est_net-params_j_correct_net
std_dev_j_net=np.sqrt(np.mean(params_j_diff_net*params_j_diff_net)-np.mean(params_j_diff_net)**2)
#print(std_dev_j_net)
mean_error_J_net=np.mean(np.abs(params_j_est_net-params_j_correct_net))
#print(mean_error_J_net)
#nJ=np.size(params_j_diff)
params_j_diff_net*=1000
std_dev_j_net*=1000
mean_error_J_net*=1000

#load data for U
params_u1=np.loadtxt(folder+"params_u1.txt")
params_u2=np.loadtxt(folder+"params_u2.txt")
params_u3=np.loadtxt(folder+"params_u3.txt")
params_u4=np.loadtxt(folder+"params_u4.txt")
params_u5=np.loadtxt(folder+"params_u5.txt")
params_u6=np.loadtxt(folder+"params_u6.txt")
params_u7=np.loadtxt(folder+"params_u7.txt")
params_u8=np.loadtxt(folder+"params_u8.txt")
params_u_correct_net=np.hstack([params_u1[:,0], params_u2[:,0], params_u3[:,0], params_u4[:,0], params_u5[:,0], params_u6[:,0], params_u7[:,0], params_u8[:,0]])
params_u_est_net=np.hstack([params_u1[:,1], params_u2[:,1], params_u3[:,1], params_u4[:,1], params_u5[:,1], params_u6[:,1], params_u7[:,1], params_u8[:,1]])
params_u_diff_net=params_u_est_net-params_u_correct_net
std_dev_u_net=np.sqrt(np.mean(params_u_diff_net*params_u_diff_net)-np.mean(params_u_diff_net)**2)
#print(std_dev_u_net)
mean_error_U_net=np.mean(np.abs(params_u_est_net-params_u_correct_net))
#print(mean_error_U_net)
#nU=np.size(params_u_diff)




# In[11]:


params_j_diff/=1000.0
std_dev_j/=1000.0
params_j_diff_2k5/=1000.0
std_dev_j_2k5/=1000.0
params_j_diff_net/=1000.0
std_dev_j_net/=1000.0
params_j_diff_net_2k5/=1000.0
std_dev_j_net_2k5/=1000.0


# In[12]:






if 1==1:

    fig = plt.figure(dpi=300)
    #ax = fig.add_subplot(121)
                          
    fig.set_size_inches(3.40457, 2.8)
    font = {'size' : 8}
    mpl.rc('font', **font)

    fig.subplots_adjust(bottom = 0.18, top=1, left=0.17, right=0.97, 
                        hspace=0.26,wspace=0.1)

    #############################
    # min and max of color axis #
    #############################


    ##########
    # Plot j 20k bayes #
    ##########

    ax = fig.add_subplot(224)
    
    for axx in [ax]:
        for axis in ['top','bottom','left','right']:
            axx.spines[axis].set_linewidth(0.5)
        axx.tick_params(width=0.5, which='both')
        axx.tick_params(length=2.5, which='major')
        axx.tick_params(length=1.5, which='minor')
        
    ax.hist(params_j_diff,bins='auto',alpha=0.9, rwidth=0.85, orientation='horizontal', color=plt.cm.Reds(0.25), label=r'Bayes 20000')

    #ax2.plot(np.array([-std_dev_U+np.mean(params_u_diff), -std_dev_U+np.mean(params_u_diff)]),np.array([0,300]),ls='--',color=plt.cm.Greys(0.4))
    #ax2.plot(np.array([std_dev_U+np.mean(params_u_diff), std_dev_U+np.mean(params_u_diff)]),np.array([0,300]),ls='--',color=plt.cm.Greys(0.4))
    #ax.plot(np.array([0,350]),np.array([-mean_error_J+np.mean(params_j_diff), -mean_error_J+np.mean(params_j_diff)]),linewidth=0.7,ls='--',color=plt.cm.Greys(0.4))
    #ax.plot(np.array([0,350]),np.array([mean_error_J+np.mean(params_j_diff), mean_error_J+np.mean(params_j_diff)]),linewidth=0.7,ls='--',color=plt.cm.Greys(0.4))
    ax.plot(np.array([0,38]),np.array([-std_dev_j+np.mean(params_j_diff), -std_dev_j+np.mean(params_j_diff)]),linewidth=0.7,ls='--',color=plt.cm.Greys(0.4))
    ax.plot(np.array([0,38]),np.array([std_dev_j+np.mean(params_j_diff), std_dev_j+np.mean(params_j_diff)]),linewidth=0.7,ls='--',color=plt.cm.Greys(0.4))


    ax.legend(loc=1, fontsize=6, frameon=False)
    #ax.ticklabel_format(style='plain')
    ax.set_xlim(0,40)

    ax.set_ylim(-0.0075,0.0075)
    #ax.set_ylabel(r'$J_{est}-J_{correct}$') #, labelpad=-1)
    ax.set_xlabel('occurence')
    ax.tick_params(axis='y', which='major', labelleft=False)

    ##########
    # Plot J #
    ##########

    cmap = plt.cm.Reds

  
    ###################
    # Manipulate size #
    ###################

    


    ##############
    # Colorbar J 2p5k bayes #
    ##############
    ax = fig.add_subplot(222)

    
    for axx in [ax]:
        for axis in ['top','bottom','left','right']:
            axx.spines[axis].set_linewidth(0.5)
        axx.tick_params(width=0.5, which='both')
        axx.tick_params(length=2.5, which='major')
        axx.tick_params(length=1.5, which='minor')
    
    
    ax.hist(params_j_diff_2k5,bins='auto',alpha=0.9, rwidth=0.85, orientation='horizontal', color=plt.cm.Reds(0.25), label=r'Bayes 2500')

    #ax2.plot(np.array([-std_dev_U+np.mean(params_u_diff), -std_dev_U+np.mean(params_u_diff)]),np.array([0,300]),ls='--',color=plt.cm.Greys(0.4))
    #ax2.plot(np.array([std_dev_U+np.mean(params_u_diff), std_dev_U+np.mean(params_u_diff)]),np.array([0,300]),ls='--',color=plt.cm.Greys(0.4))
    #ax.plot(np.array([0,350]),np.array([-mean_error_J+np.mean(params_j_diff), -mean_error_J+np.mean(params_j_diff)]),linewidth=0.7,ls='--',color=plt.cm.Greys(0.4))
    #ax.plot(np.array([0,350]),np.array([mean_error_J+np.mean(params_j_diff), mean_error_J+np.mean(params_j_diff)]),linewidth=0.7,ls='--',color=plt.cm.Greys(0.4))
    ax.plot(np.array([0,38]),np.array([-std_dev_j_2k5+np.mean(params_j_diff_2k5), -std_dev_j_2k5+np.mean(params_j_diff_2k5)]),linewidth=0.7,ls='--',color=plt.cm.Greys(0.4))
    ax.plot(np.array([0,38]),np.array([std_dev_j_2k5+np.mean(params_j_diff_2k5), std_dev_j_2k5+np.mean(params_j_diff_2k5)]),linewidth=0.7,ls='--',color=plt.cm.Greys(0.4))


    ax.legend(loc=1, fontsize=6, frameon=False)
    #ax.ticklabel_format(style='plain')
    ax.set_ylim(-0.0075,0.0075)
    ax.set_xlim(0,40)

    #ax.set_ylabel(r'$J_{est}-J_{correct}$') #, labelpad=-1)
    #ax.tick_params(axis='x', which='major', labelbottom=False)
    ax.tick_params(axis='y', which='major', labelleft=False)

    #ax.set_xlabel('occurence')
    #ax.set_xlabel('occurence')
    #ax.yticks([])
    ##############
    # Colorbar J #
    ##############

    #print(params_u_diff)
    #print(params_u_diff_net)
    
    
    
    ##############
    # Bar plot J net 20k #
    ##############
    
    ax = fig.add_subplot(223)
    
    for axx in [ax]:
        for axis in ['top','bottom','left','right']:
            axx.spines[axis].set_linewidth(0.5)
        axx.tick_params(width=0.5, which='both')
        axx.tick_params(length=2.5, which='major')
        axx.tick_params(length=1.5, which='minor')
    
    
    
    ax.hist(params_j_diff_net,bins='auto',alpha=0.9, rwidth=0.85, orientation='horizontal', color=plt.cm.Reds(0.5), label=r'NN 20000')

    #ax2.plot(np.array([-std_dev_U+np.mean(params_u_diff), -std_dev_U+np.mean(params_u_diff)]),np.array([0,300]),ls='--',color=plt.cm.Greys(0.4))
    #ax2.plot(np.array([std_dev_U+np.mean(params_u_diff), std_dev_U+np.mean(params_u_diff)]),np.array([0,300]),ls='--',color=plt.cm.Greys(0.4))
    #ax.plot(np.array([0,350]),np.array([-mean_error_J+np.mean(params_j_diff), -mean_error_J+np.mean(params_j_diff)]),linewidth=0.7,ls='--',color=plt.cm.Greys(0.4))
    #ax.plot(np.array([0,350]),np.array([mean_error_J+np.mean(params_j_diff), mean_error_J+np.mean(params_j_diff)]),linewidth=0.7,ls='--',color=plt.cm.Greys(0.4))
    ax.plot(np.array([0,38]),np.array([-std_dev_j_net+np.mean(params_j_diff_net), -std_dev_j_net+np.mean(params_j_diff_net)]),linewidth=0.7,ls='--',color=plt.cm.Greys(0.4))
    ax.plot(np.array([0,38]),np.array([std_dev_j_net+np.mean(params_j_diff_net), std_dev_j_net+np.mean(params_j_diff_net)]),linewidth=0.7,ls='--',color=plt.cm.Greys(0.4))


    ax.legend(loc=1, fontsize=6, frameon=False)
    #ax.ticklabel_format(style='plain')
    ax.set_xlim(0,40)

    ax.set_ylim(-0.0075,0.0075)
    ax.set_ylabel(r'$J_{est}-J_{correct}$') #, labelpad=-1)
    ax.set_xlabel('occurence')

    ##############
    # Bar plot j net 2p5k #
    ##############
    ax = fig.add_subplot(221)

    for axx in [ax]:
        for axis in ['top','bottom','left','right']:
            axx.spines[axis].set_linewidth(0.5)
        axx.tick_params(width=0.5, which='both')
        axx.tick_params(length=2.5, which='major')
        axx.tick_params(length=1.5, which='minor')
    
   
    
    ax.hist(params_j_diff_net_2k5,bins='auto',alpha=0.9, rwidth=0.85, orientation='horizontal', color=plt.cm.Reds(0.5), label=r'NN 2500')

    #ax2.plot(np.array([-std_dev_U+np.mean(params_u_diff), -std_dev_U+np.mean(params_u_diff)]),np.array([0,300]),ls='--',color=plt.cm.Greys(0.4))
    #ax2.plot(np.array([std_dev_U+np.mean(params_u_diff), std_dev_U+np.mean(params_u_diff)]),np.array([0,300]),ls='--',color=plt.cm.Greys(0.4))
    #ax.plot(np.array([0,350]),np.array([-mean_error_J+np.mean(params_j_diff), -mean_error_J+np.mean(params_j_diff)]),linewidth=0.7,ls='--',color=plt.cm.Greys(0.4))
    #ax.plot(np.array([0,350]),np.array([mean_error_J+np.mean(params_j_diff), mean_error_J+np.mean(params_j_diff)]),linewidth=0.7,ls='--',color=plt.cm.Greys(0.4))
    ax.plot(np.array([0,38]),np.array([-std_dev_j_net_2k5+np.mean(params_j_diff_net_2k5), -std_dev_j_net_2k5+np.mean(params_j_diff_net_2k5)]),linewidth=0.7,ls='--',color=plt.cm.Greys(0.4))
    ax.plot(np.array([0,38]),np.array([std_dev_j_net_2k5+np.mean(params_j_diff_net_2k5), std_dev_j_net_2k5+np.mean(params_j_diff_net_2k5)]),linewidth=0.7,ls='--',color=plt.cm.Greys(0.4))


    ax.legend(loc=1, fontsize=6, frameon=False)
    #ax.ticklabel_format(style='plain')
    ax.set_xlim(0,40)

    ax.set_ylim(-0.0075,0.0075)
    ax.set_ylabel(r'$J_{est}-J_{correct}$') #, labelpad=-1)
    #ax.set_xlabel('occurence')
    #ax.tick_params(axis='x', which='major', labelbottom=False)

    ################
    # panel labels #
    ################
    #fig.text(0.14,0.625,r'$\times$1e-3',fontsize=8)

    #fig.text(0.03, 0.96, '(a)')
    #fig.text(0.03, 0.65, '(b)')
    #fig.text(0.53, 0.65, '(c)')

    ###############
    # save figure #
    ###############
    fig.tight_layout()
    fig.savefig('hist_comparison_Bayes_NN_J.pdf')
    plt.show()

# In[13]:




if 1==1:

    fig = plt.figure(dpi=300)
    #ax = fig.add_subplot(121)
                          
    fig.set_size_inches(3.40457, 2.8)
    font = {'size' : 8}
    mpl.rc('font', **font)

    fig.subplots_adjust(bottom = 0.18, top=1, left=0.17, right=0.97, 
                        hspace=0.26,wspace=0.1)

    #############################
    # min and max of color axis #
    #############################


    ##########
    # Plot j 20k bayes #
    ##########

    ax = fig.add_subplot(224)
    
    for axx in [ax]:
        for axis in ['top','bottom','left','right']:
            axx.spines[axis].set_linewidth(0.5)
        axx.tick_params(width=0.5, which='both')
        axx.tick_params(length=2.5, which='major')
        axx.tick_params(length=1.5, which='minor')
        
    ax.hist(params_u_diff,bins='auto',alpha=0.9, rwidth=0.85, orientation='horizontal', color=plt.cm.Blues(0.4), label=r'Bayes 20000')

    #ax2.plot(np.array([-std_dev_U+np.mean(params_u_diff), -std_dev_U+np.mean(params_u_diff)]),np.array([0,300]),ls='--',color=plt.cm.Greys(0.4))
    #ax2.plot(np.array([std_dev_U+np.mean(params_u_diff), std_dev_U+np.mean(params_u_diff)]),np.array([0,300]),ls='--',color=plt.cm.Greys(0.4))
    #ax.plot(np.array([0,350]),np.array([-mean_error_J+np.mean(params_j_diff), -mean_error_J+np.mean(params_j_diff)]),linewidth=0.7,ls='--',color=plt.cm.Greys(0.4))
    #ax.plot(np.array([0,350]),np.array([mean_error_J+np.mean(params_j_diff), mean_error_J+np.mean(params_j_diff)]),linewidth=0.7,ls='--',color=plt.cm.Greys(0.4))
    ax.plot(np.array([0,30]),np.array([-std_dev_u+np.mean(params_u_diff), -std_dev_u+np.mean(params_u_diff)]),linewidth=0.7,ls='--',color=plt.cm.Greys(0.4))
    ax.plot(np.array([0,30]),np.array([std_dev_u+np.mean(params_u_diff), std_dev_u+np.mean(params_u_diff)]),linewidth=0.7,ls='--',color=plt.cm.Greys(0.4))


    ax.legend(loc=1, fontsize=6, frameon=False)
    #ax.ticklabel_format(style='plain')
    ax.set_xlim(0,38)

    ax.set_ylim(-0.02,0.02)
    #ax.set_ylabel(r'$J_{est}-J_{correct}$') #, labelpad=-1)
    ax.set_xlabel('occurence')
    ax.tick_params(axis='y', which='major', labelleft=False)

    ##########
    # Plot J #
    ##########

    cmap = plt.cm.Reds

  
    ###################
    # Manipulate size #
    ###################

    


    ##############
    # Colorbar J 2p5k bayes #
    ##############
    ax = fig.add_subplot(222)

    
    for axx in [ax]:
        for axis in ['top','bottom','left','right']:
            axx.spines[axis].set_linewidth(0.5)
        axx.tick_params(width=0.5, which='both')
        axx.tick_params(length=2.5, which='major')
        axx.tick_params(length=1.5, which='minor')
    
    
    ax.hist(params_u_diff_2k5,bins='auto',alpha=0.9, rwidth=0.85, orientation='horizontal', color=plt.cm.Blues(0.4), label=r'Bayes 2500')

    #ax2.plot(np.array([-std_dev_U+np.mean(params_u_diff), -std_dev_U+np.mean(params_u_diff)]),np.array([0,300]),ls='--',color=plt.cm.Greys(0.4))
    #ax2.plot(np.array([std_dev_U+np.mean(params_u_diff), std_dev_U+np.mean(params_u_diff)]),np.array([0,300]),ls='--',color=plt.cm.Greys(0.4))
    #ax.plot(np.array([0,350]),np.array([-mean_error_J+np.mean(params_j_diff), -mean_error_J+np.mean(params_j_diff)]),linewidth=0.7,ls='--',color=plt.cm.Greys(0.4))
    #ax.plot(np.array([0,350]),np.array([mean_error_J+np.mean(params_j_diff), mean_error_J+np.mean(params_j_diff)]),linewidth=0.7,ls='--',color=plt.cm.Greys(0.4))
    ax.plot(np.array([0,30]),np.array([-std_dev_u_2k5+np.mean(params_u_diff_2k5), -std_dev_u_2k5+np.mean(params_u_diff_2k5)]),linewidth=0.7,ls='--',color=plt.cm.Greys(0.4))
    ax.plot(np.array([0,30]),np.array([std_dev_u_2k5+np.mean(params_u_diff_2k5), std_dev_u_2k5+np.mean(params_u_diff_2k5)]),linewidth=0.7,ls='--',color=plt.cm.Greys(0.4))


    ax.legend(loc=1, fontsize=6, frameon=False)
    #ax.ticklabel_format(style='plain')
    ax.set_ylim(-0.02,0.02)
    ax.set_xlim(0,38)

    #ax.set_ylabel(r'$J_{est}-J_{correct}$') #, labelpad=-1)
    #ax.tick_params(axis='x', which='major', labelbottom=False)
    ax.tick_params(axis='y', which='major', labelleft=False)

    #ax.set_xlabel('occurence')
    #ax.set_xlabel('occurence')
    #ax.yticks([])
    ##############
    # Colorbar J #
    ##############

    #print(params_u_diff)
    #print(params_u_diff_net)
    
    
    
    ##############
    # Bar plot J net 20k #
    ##############
    
    ax = fig.add_subplot(223)
    
    for axx in [ax]:
        for axis in ['top','bottom','left','right']:
            axx.spines[axis].set_linewidth(0.5)
        axx.tick_params(width=0.5, which='both')
        axx.tick_params(length=2.5, which='major')
        axx.tick_params(length=1.5, which='minor')
    
    
    
    ax.hist(params_u_diff_net,bins='auto',alpha=0.9, rwidth=0.85, orientation='horizontal', color=plt.cm.Blues(0.7), label=r'NN 20000')

    #ax2.plot(np.array([-std_dev_U+np.mean(params_u_diff), -std_dev_U+np.mean(params_u_diff)]),np.array([0,300]),ls='--',color=plt.cm.Greys(0.4))
    #ax2.plot(np.array([std_dev_U+np.mean(params_u_diff), std_dev_U+np.mean(params_u_diff)]),np.array([0,300]),ls='--',color=plt.cm.Greys(0.4))
    #ax.plot(np.array([0,350]),np.array([-mean_error_J+np.mean(params_j_diff), -mean_error_J+np.mean(params_j_diff)]),linewidth=0.7,ls='--',color=plt.cm.Greys(0.4))
    #ax.plot(np.array([0,350]),np.array([mean_error_J+np.mean(params_j_diff), mean_error_J+np.mean(params_j_diff)]),linewidth=0.7,ls='--',color=plt.cm.Greys(0.4))
    ax.plot(np.array([0,30]),np.array([-std_dev_u_net+np.mean(params_u_diff_net), -std_dev_u_net+np.mean(params_u_diff_net)]),linewidth=0.7,ls='--',color=plt.cm.Greys(0.4))
    ax.plot(np.array([0,30]),np.array([std_dev_u_net+np.mean(params_u_diff_net), std_dev_u_net+np.mean(params_u_diff_net)]),linewidth=0.7,ls='--',color=plt.cm.Greys(0.4))


    ax.legend(loc=1, fontsize=6, frameon=False)
    #ax.ticklabel_format(style='plain')
    ax.set_xlim(0,38)

    ax.set_ylim(-0.02,0.02)
    ax.set_ylabel(r'$U_{est}-U_{correct}$') #, labelpad=-1)
    ax.set_xlabel('occurence')

    ##############
    # Bar plot j net 2p5k #
    ##############
    ax = fig.add_subplot(221)

    for axx in [ax]:
        for axis in ['top','bottom','left','right']:
            axx.spines[axis].set_linewidth(0.5)
        axx.tick_params(width=0.5, which='both')
        axx.tick_params(length=2.5, which='major')
        axx.tick_params(length=1.5, which='minor')
    
   
    
    ax.hist(params_u_diff_net_2k5,bins='auto',alpha=0.9, rwidth=0.85, orientation='horizontal', color=plt.cm.Blues(0.7), label=r'NN 2500')

    #ax2.plot(np.array([-std_dev_U+np.mean(params_u_diff), -std_dev_U+np.mean(params_u_diff)]),np.array([0,300]),ls='--',color=plt.cm.Greys(0.4))
    #ax2.plot(np.array([std_dev_U+np.mean(params_u_diff), std_dev_U+np.mean(params_u_diff)]),np.array([0,300]),ls='--',color=plt.cm.Greys(0.4))
    #ax.plot(np.array([0,350]),np.array([-mean_error_J+np.mean(params_j_diff), -mean_error_J+np.mean(params_j_diff)]),linewidth=0.7,ls='--',color=plt.cm.Greys(0.4))
    #ax.plot(np.array([0,350]),np.array([mean_error_J+np.mean(params_j_diff), mean_error_J+np.mean(params_j_diff)]),linewidth=0.7,ls='--',color=plt.cm.Greys(0.4))
    ax.plot(np.array([0,30]),np.array([-std_dev_u_net_2k5+np.mean(params_u_diff_net_2k5), -std_dev_u_net_2k5+np.mean(params_u_diff_net_2k5)]),linewidth=0.7,ls='--',color=plt.cm.Greys(0.4))
    ax.plot(np.array([0,30]),np.array([std_dev_u_net_2k5+np.mean(params_u_diff_net_2k5), std_dev_u_net_2k5+np.mean(params_u_diff_net_2k5)]),linewidth=0.7,ls='--',color=plt.cm.Greys(0.4))


    ax.legend(loc=1, fontsize=6, frameon=False)
    #ax.ticklabel_format(style='plain')
    ax.set_xlim(0,38)

    ax.set_ylim(-0.02,0.02)
    ax.set_ylabel(r'$U_{est}-U_{correct}$') #, labelpad=-1)
    #ax.set_xlabel('occurence')
    #ax.tick_params(axis='x', which='major', labelbottom=False)

    ################
    # panel labels #
    ################
    #fig.text(0.14,0.625,r'$\times$1e-3',fontsize=8)

    #fig.text(0.03, 0.96, '(a)')
    #fig.text(0.03, 0.65, '(b)')
    #fig.text(0.53, 0.65, '(c)')

    ###############
    # save figure #
    ###############
    fig.tight_layout()
    fig.savefig('hist_comparison_Bayes_NN_U.pdf')
    plt.show()

# In[ ]:




