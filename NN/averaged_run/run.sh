
julia generate_snapshots_2p5k.jl
julia convert_to_expvalues_2p5k.jl

echo ' '
echo 'Estimate Bose-Hubbard parameters...'
echo ' '

cd network_run_2p5k
python eval_j12.py
python eval_j15.py
python eval_j23.py
python eval_j26.py
python eval_j34.py
python eval_j37.py
python eval_j48.py
python eval_j56.py
python eval_j67.py
python eval_j78.py
python eval_u1.py
python eval_u2.py
python eval_u3.py
python eval_u4.py
python eval_u5.py
python eval_u6.py
python eval_u7.py
python eval_u8.py
python eval_mu2diff.py
python eval_mu3diff.py
python eval_mu4diff.py
python eval_mu5diff.py
python eval_mu6diff.py
python eval_mu7diff.py
python eval_mu8diff.py
cd ../

echo ' '
echo 'Parameters estimated.'
echo ' '

julia generate_snapshots_20k.jl
julia convert_to_expvalues_20k.jl

cd network_run_20k
python eval_j12.py
python eval_j15.py
python eval_j23.py
python eval_j26.py
python eval_j34.py
python eval_j37.py
python eval_j48.py
python eval_j56.py
python eval_j67.py
python eval_j78.py
python eval_u1.py
python eval_u2.py
python eval_u3.py
python eval_u4.py
python eval_u5.py
python eval_u6.py
python eval_u7.py
python eval_u8.py
python eval_mu2diff.py
python eval_mu3diff.py
python eval_mu4diff.py
python eval_mu5diff.py
python eval_mu6diff.py
python eval_mu7diff.py
python eval_mu8diff.py
cd ../

echo ' '
echo 'Parameters estimated.'
echo ' '

python plot.py
