#=
Author: Guliuxin Jin, Agnes Valenti
Date: 01.03.2021

This code generates 'mt' number of projective measurement snapshots from a time-evolution under a
Bose-Hubbard Hamiltonian with randomly generated parameters within the intervals
J_ij in [0.995,1.005]
U_i in [1.99,2.01]
mu_i in [0.995,1.005]
=#



# generate ML data 
# detect if using SLURM
const IN_SLURM = "SLURM_JOBID" in keys(ENV)

# load packages
using Distributed
IN_SLURM && using ClusterManagers

# Here we create our parallel julia processes
if IN_SLURM
    pids = addprocs_slurm(parse(Int, ENV["SLURM_NTASKS"]))
    print("\n")
else
    pids = addprocs()
end
#println("workers = ", workers())

@everywhere using LinearAlgebra, Random, DelimitedFiles

###################################################################
##################                             ####################
##################     Declare  parameters     ####################
##################                             ####################
###################################################################

hbar = 1 ;  N = 4 ;   M = 8 ;  ###  N: number of atoms, M: number of lattice sites

#t_evl: time after that measurement snapshots are taken
t_evl=200;
timesteps= 1; 

ini_st = 150;  # initial state = Fock state No.150  time evolution:200*hbar

#number of measurement sets
measure_set= 1; 

#number of snapshots
mt = 2500; 

parm_order = (((1),(5,2)), ((5),(6,1)), ((6),(2,5,7)), ((2),(1,6,3)), ((3),(2,7,4)), ((7),(3,6,8)), ((8),(7,4)), ((4),(8,3)) );


print("Generate measurement snapshots...")

### Function 1: Define the fock basis collection
function basis(N,M)  # collection of all Fock basis for N particles in M lattices.
    hbt=binomial(M+N-1,M-1)  #how many different possible configurations for N particles, M lattice sites
# --------------------     function basis(s,hbt,bss)   --------------------    
    bss_outcome = []
    a = zeros(Int64,M)
    for i in 1:(N+1)^M
        if sum(a)==N
            b=copy(a)
            push!(bss_outcome,b)
        end        
        a[M]+=1
        for j in 0:M-2
            if a[M-j]==N+1
                a[M-j]=0
                a[M-j-1] +=1
            end 
        end
    end
    bss_outcome
# --------------------     function MB_state(s,hbt,bss)   --------------------
    bsslenth = length(bss_outcome) 
    fullmb = []
    for s in 1:bsslenth
        mbstate = zeros(Int64, hbt)
        mbstate[s]=1
        push!(fullmb,mbstate)
    end
    return bss_outcome, fullmb
end
bss, MB_state = basis(N,M); ##### this is one potential global variables!

###################################################################################
### Function 3: probs of given parms[i]. only evaluate 1 group once. Each time: parm = parms[i]
@everywhere function single_prob(hop,u_int,mu,N,M,bss,MB_state,time,ini_st,hbar)
    # -----          Hamiltonian:  H2D_kin(hop , N , M , hbt, bss)        ---------------
    hbt=binomial(M+N-1,M-1);
    H1 = zeros(Float64,hbt,hbt) # initialize the kinetic hamiltonian
    Mhalf = convert(Int, M/2)   # length of the lattice = 4.
    for v in 1: hbt             # for each basis state |v>, we calculate one column in H_kin
        for j in 1:M           # consider j-th site of the given basis |v>
            if bss[v][j]>0     # if the j-th site is not empty, then run the following.
                if j != 1&& j !=(M/2+1)               #  hop to left <-
                    vector1= copy(bss[v])            #  initialize the state.
                    vector1[j]= vector1[j]-1         #   a_j annihilate 1 particle at site j
                    vector1[j-1]= vector1[j-1]+1     #    a†i create 1 particle at site i=j-1
                    idx1 = findall(x->x==vector1, bss) # find the index of new basis |vector1>
                    H1[idx1[1],v]=copy( -hop[j,j-1]*sqrt((bss[v][j-1]+1)*bss[v][j]) )# coppresponding matrix element value
                end
                if j != M/2 && j != M              # hop to right ->
                    vector2= copy(bss[v]) 
                    vector2[j]= vector2[j]-1       # a_j annihilate 1 particle at site j
                    vector2[j+1]= vector2[j+1]+1   # a†i create 1 particle at site j+1
                    idx2 = findall(x->x==vector2, bss) # find the index of new basis |vector2>
                    H1[idx2[1],v] = copy(- hop[j,j+1] *sqrt((bss[v][j+1]+1)*bss[v][j]))
                end
                if j in 1:M/2                     # hop to bottom from site j to j+M/2
                    vector3 = copy(bss[v]) 
                    vector3[j]= vector3[j]-1      #   a_j annihilate 1 particle at site j
                    vector3[j+Mhalf]= vector3[j+Mhalf]+1  #    a†i create 1 particle at site i=j+M/2
                    idx3 = findall(x->x==vector3, bss) # find the index of new basis |vector1>
                    H1[idx3[1],v] = copy(-(hop[j,j+Mhalf])*sqrt((bss[v][j+Mhalf]+1)*bss[v][j]) )# assign the coppresponding matrix element values
                end
                if j in (M/2+1):M                # hop to top from site j to j-M/2
                    vector4 = copy(bss[v]) 
                    vector4[j]= vector4[j]-1      #   a_j annihilate 1 particle at site j
                    vector4[j-Mhalf]= vector4[j-Mhalf]+1  #    a†i create 1 particle at site i=j+M/2
                    idx4 = findall(x->x==vector4, bss) # find the index of new basis |vector1>
                    H1[idx4[1],v] = copy( -(hop[j,j-Mhalf])*sqrt((bss[v][j-Mhalf]+1)*bss[v][j])) # assign the coppresponding matrix element values
                end                
            end
        end
    end
    H1
    # ------------------ H2D_int( u_int,N,M,hbt, bss) ------------------
    H2=zeros(Float64,hbt,hbt)
    for v in 1:hbt
        sum1=sum( (u_int[i]/2)*bss[v][i]*(bss[v][i]-1) for i in 1:M) 
        H2[v,v] = sum1 
    end
    H2
    # ------------------    H2D_chem( mu,N,M,hbt, bss)   ------------------
    H3=zeros(Float64,hbt,hbt)
    for v in 1:hbt
        sum2 = sum( mu[i]*bss[v][i] for i in 1:M) 
        H3[v,v] = sum2
    end
    H3
    H_total = H1+H2-H3    
    #---------------  all the eigen values and eigen vectors.   ----------------
    eig_vals1, eig_vecs1 = eigen(H_total)   #hermitesch?
    #------------------\Psi_{E_x}><Psi_{E_x}\  basis transition for x-th eigenstate, x in hbt.---------------
    basis_trans_bys1 = []
    for x in 1: hbt
        basis_trans = eig_vecs1[: ,x] * reshape(eig_vecs1[:,x],1,:)  
        push!( basis_trans_bys1 , basis_trans)     
    end
    #------------------  state_evolution_bys1    ---------------
    #     MB_state[ini_st] is the  manybody state of chosen initial state, MB_state[2] = 02000000...
    seb = sum( exp(-1*im*eig_vals1[i]*(time/hbar))*basis_trans_bys1[i]*MB_state[ini_st]  for i in 1:hbt)
    #------------------    prob_total, prob_sort_total, prob_accum_total   ---------------
    prob = [(abs(dot(MB_state[i],seb)))^2 for i in 1:hbt]  # prob of occurrence of each Fock basis in the current state after evolution
    return prob
end

##################################################################################
### Function 4:  generate the measurement by the correct parameter we assign. it contains mt number of measurements.
function measurement(;prob_c, mt)
    hbt = length(prob_c)
    prob_sort = sort(prob_c , rev=true);
    prob_accum = [ sum( prob_sort[j] for j in 1:p)  for p in 1:hbt]
    rnds = rand(mt);
    measure = zeros(Int64,mt)
    for i in 1:mt                   
            prob_copy = copy(prob_accum);
            push!(prob_copy, rnds[i])             
            sort!(prob_copy)                      
            idx = findall(x->x== rnds[i], prob_copy )[1]      
            measure[i] = findall(x->x== prob_sort[idx] , prob_c)[1]
    end
    return measure
end

j_midval = 1;  u_midval = 2;  mu_midval = 1;
######################  initial the correct parameter    #################################
j_rand() =  rand().*0.01 .+(0.995*j_midval) # distributed region [0, 0.01) + 0.995 = [0.995, 1.005)
u_rand() =  rand().*0.02 .+(1.99) # distributed region [0, 0.2) + 1.99 = [1.99, 2.01)
mu_rand() = rand().*0.01 .+(0.995*mu_midval) # distributed region [0, 0.01) + 0.995 = [0.995, 1.005)

function parm_correct(;M, parm_order)  # generate a group of correct parameter
    jout = zeros(Float64,M,M);
    uout = zeros(Float64,M);
    muout = zeros(Float64,M);

    parmleng = length(parm_order);
    for i in 1:parmleng
        group = parm_order[i]
        center = parm_order[i][1]     # example: 6
        neighbour = parm_order[i][2]  # example: (2, 5, 7)
        for j in neighbour
            jout[center,j] = jout[j,center] = j_rand() 
        end
        uout[center] = u_rand();
        muout[center] = mu_rand()
    end
    return jout, uout, muout
end




#  num_parms: number of parameter groups, each randomly generated.
# this function generate all the data
function data_gen(;num_parms)
    j_c_total = []; u_c_total = []; mu_c_total = [];
    for i in 1: num_parms
        j_c,u_c,mu_c = parm_correct(M=M, parm_order=parm_order)
        #j_c = readdlm("params_j.txt", Float64)
        #u_c = readdlm("params_u.txt", Float64)
        #mu_c = readdlm("params_mu.txt", Float64)
        #j_c[2,6]=jarray[i]
        #j_c[6,2]=jarray[i]
        #######################################################
        push!(j_c_total,j_c)
        push!(u_c_total, u_c)
        push!(mu_c_total, mu_c)
    end
    prob_c_total = pmap(single_prob, j_c_total, u_c_total ,mu_c_total,[N for i in 1:num_parms],[M for i in 1:num_parms],[bss for i in 1:num_parms],[MB_state for i in 1:num_parms],[t_evl for i in 1:num_parms],[ini_st for i in 1:num_parms],[hbar for i in 1:num_parms])
    
    measure_c_total = []
    for i in 1:num_parms
        prob_c = prob_c_total[i]
        measure_c = [];
        measure_c = [ measurement(;prob_c = prob_c, mt = mt)    for i in 1:measure_set];
        #print("size measure_c element: ",measure_c[1][1],"\n")
        push!(measure_c_total, measure_c)
    end
    #print("size measure_c_total: ",size(measure_c_total),"\n")
    return j_c_total, u_c_total, mu_c_total, measure_c_total
end

###       Generate the measurement based on the above correct parameters      ####
@time begin
    j_t,u_t,mu_t,measures = data_gen(num_parms=timesteps);
end

print("\n","Snapshots generated.","\n","\n")

for i in 1:timesteps
    j_loop=j_t[i]
    #print(j_loop, "\n")
    u_loop=u_t[i]
    mu_loop=mu_t[i]
    measure_loop=measures[i]
    writedlm("data/jvalues_correct.txt", j_loop)
    writedlm("data/uvalues_correct.txt", u_loop)
    writedlm("data/muvalues_correct.txt", mu_loop)
    writedlm("data/snapshots.txt", measure_loop)
    #open("mydata_generate$i.txt", "w") do io
        #write(io, "$j_loop, \n, $u_loop, \n,$mu_loop, \n,$measure_loop " ) #$j_t, $u_t, $mu_t, $measures
    #end
end;
