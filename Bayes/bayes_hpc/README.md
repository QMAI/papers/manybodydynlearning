# Bayes HPC
- This directory contains the full Bayes inference code to estimate all 10 $J_{ij}$ and 8 $U_i$. An HPC/cluster/supercomputer is needed. 
- Package requirement:  julia (version 1.5 or higher) and related packaged are needed.


The code package contains two part:
- Data generation: `generate_snapshots.jl`, same as in the directory `NN`. 
- Bayesian inference: `bayes_hpc.jl`.


## Usage
### Data generation
- This part you can run on your local pc.
- We run a simulation of Bose-Hubbard Hamiltonian on 8 lattice sites with 4 atoms with randomly generated parameters. 
- In order to run the data generation `generate_snapshots.jl`, run the script `run.sh`. A simulation of such an estimation process can be performed by enabling the script via `chmod +x script.sh` and executing via `./run.sh`.
- File in the folder `data`: 
- `snapshots.txt`: Generated snapshots, saved in the form of Fock basis labeling.
- `jvalues_correct.txt`, `uvalues_correct.txt`, `muvalues_correct.txt`: randomly chosen parameters, used to generate snapshots.



### Bayesian estimation 
- The Bayesian estimation part is contained in the file:`bayes_hpc.jl`. The code will read `snapshots.txt` saved in the folder `data`.
- This part you need to run in parallelization on a cluster.
- The parallelization part is tailored to run on clusters using SLURM system. Running on different system needs to change this part accordingly.
- the output folder is `bayes_data`. It contains: `final_J.txt`,`final_U.txt`,10 of `update_j_ij.txt`and 8 of`update_u_i.txt`.
- `final_J.txt`: records the final estimated J in a matrix form of $8\times 8$.
- `final_U.txt`: records the final estimated U in an 1D arrray of 8 elements .
- `update_j_ij.txt` : records the evolution of $J_ij$ values through estimations.
- `update_u_i.txt` : records the evolution of $U_i$ values through estimations.