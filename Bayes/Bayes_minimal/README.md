# Bayes minimal:
- we provide a minimal example, where users can run bayesian estimation on their personal computers.
    - Instead of the full 18 parameters, we only estimate one `J` and one `U` due to the limited computational capacity.
- The notebook 'Bayes_minimal.ipynb' contains the following three parts:
    1. Initialize the parallelization and algorithm related parameters
    2. Experiment data generation or read
        2.1 Data generation 
        2.2 Read existing data
    3. Bayes inference of one `J_ij` and one `U_i`
- Measurement snapshots are saved in directory 'data'
    
## 1. Initialize the parallelization algorithm related parameters

- This notebook can be either run on a personal computer (pc) or on a cluster. In order to exploit the computational resource, this section will detect the environment and initialize the multi-threading computing. On the cluster side, we use SLURM system as an example.
- We declare all the necessary parameters here, such as number of atoms, evolution time etc.

## 2. Experiment data generation:
   
### 2.1 Data generation 

- This section is devoted to generate the snapshots data. The combination of Section 1. and 2. is exactly the same code you can find in `generate_snapshots.jl` from the directory `manybodydynlearning//NN/single_run` in our gitlab.
- If you already have generated the snapshots and you want to perform the bayes inference on that data. Do not run Sec. 2.1, instead, you can run the Sec. 2.2 see below.

### 2.2 Read existing data:
- uncomment and run this cell when `snapshots.txt` already **exists** .
- When the names are different, change the `snapshots.txt` to the name of your snapshots file, and corrospondingly the name of files containing correct parameters.

## 3: Bayesian inference
- We only estimate one `J_ij` and one `U_i` in this bayes_minimal notebook for demonstration purpose. For a full 18 parameter estimation, please see bayes_HPC. We choose `J_26` and `U_3` as the example.
- During the estimation, all the rest parameters are fixed at the correct values.
- the output contains: `final_J.txt`,`final_U.txt`,`update_j_26.txt`and`update_u_3.txt`.
    - `final_J.txt`: records the final estimated J in a matrix form of $8\times 8$.
    - `final_U.txt`: records the final estimated U in an 1D arrray of 8 elements .
    - `update_j_26.txt` : records the evolution of `J_26` values through estimations.
    - `update_u_3.txt` : records the evolution of `U_3` values through estimations.