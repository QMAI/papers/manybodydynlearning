# Bayes estimation
- Bayes minimal: estimate only one $J$ and one $U$. can be run on a pc locally.
- Bayes hpc: estimate 18 parameters. Need to be run on a HPC/Clusters.

Please see detailed README in separate folders.